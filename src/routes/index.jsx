import accountLayout from '../layouts/Account';

// import Register from 'views/general/Pages/Register.jsx';
import Login from '../views/account/Authentication/Login';

const BASEDIR = process.env.REACT_APP_BASEDIR;
const indexRoutes = [
  { path: `${BASEDIR}/login`, name: 'Login', component: Login },
  // { path: `${BASEDIR}/register`, name: 'Register', component: Register },
  { path: '/', name: 'Staff', component: accountLayout }
];

export default indexRoutes;
