import Homepage from '../views/account/Homepage/Homepage';

import AllStaff from '../views/account/Staff/AllStaff';
import AddStaff from '../views/account/Staff/AddStaff';
import StaffProfile from '../views/account/Staff/StaffProfile';
import EditStaff from '../views/account/Staff/EditStaff';

import AllBanks from '../views/account/Bank/AllBanks';
import AddBank from '../views/account/Bank/AddBank';
import EditBank from '../views/account/Bank/EditBank';

import AllPFA from '../views/account/PFA/AllPFA';
import AddPFA from '../views/account/PFA/AddPFA';
import EditPFA from '../views/account/PFA/EditPFA';

import AllSchoolBranch from '../views/account/SchoolBranch/AllSchoolBranch';
import AddBranch from '../views/account/SchoolBranch/AddBranch';
import EditBranch from '../views/account/SchoolBranch/EditBranch';

import AllVariableAdjustment from '../views/account/VariableAdjustment/AllVariableAdjustment';
import AddVariableType from '../views/account/VariableAdjustment/AddVariableType';
import EditVariableType from '../views/account/VariableAdjustment/EditVariableType';

import VoucherHome from '../views/account/Voucher/VoucherHome';
import Payslip from '../views/account/Voucher/Payslip';
import Voucher from '../views/account/Voucher/Voucher';
import EditPayslip from '../views/account/Voucher/EditPayslip';

const BASEDIR = process.env.REACT_APP_BASEDIR;

const dashRoutes = [
  {
    path: `${BASEDIR}/home`,
    name: 'Home',
    icon: 'home',
    component: Homepage
  },
  {
    path: '#',
    name: 'Staff',
    icon: 'people',
    type: 'dropdown',
    parentid: 'staff',
    child: [
      { path: `${BASEDIR}/staff`, name: 'All Staff' },
      { path: `${BASEDIR}/add-staff`, name: 'Add New Staff' }
    ]
  },
  { path: `${BASEDIR}/staff`, component: AllStaff, type: 'child' },
  { path: `${BASEDIR}/add-staff`, component: AddStaff, type: 'child' },
  { path: `${BASEDIR}/staff/profile/:id`, exact: true, component: StaffProfile, type: 'child' },
  { path: `${BASEDIR}/staff/profile/edit/:id`, component: EditStaff, type: 'child' },

  {
    path: '#',
    name: 'Bank',
    icon: 'credit-card',
    type: 'dropdown',
    parentid: 'patients',
    child: [
      { path: `${BASEDIR}/bank`, name: 'All Banks' },
      { path: `${BASEDIR}/add-bank`, name: 'Add Bank' }
    ]
  },
  { path: `${BASEDIR}/bank`, component: AllBanks, type: 'child' },
  { path: `${BASEDIR}/add-bank`, component: AddBank, type: 'child' },
  { path: `${BASEDIR}/bank/edit/:id`, component: EditBank, type: 'child' },

  {
    path: '#',
    name: 'PFA',
    icon: 'briefcase',
    type: 'dropdown',
    parentid: 'patients',
    child: [
      { path: `${BASEDIR}/pfa`, name: 'All PFAs' },
      { path: `${BASEDIR}/add-pfa`, name: 'Add PFA' }
    ]
  },
  { path: `${BASEDIR}/pfa`, component: AllPFA, type: 'child' },
  { path: `${BASEDIR}/add-pfa`, component: AddPFA, type: 'child' },
  { path: `${BASEDIR}/pfa/edit/:id`, component: EditPFA, type: 'child' },

  {
    path: '#',
    name: 'School Branch',
    icon: 'share',
    type: 'dropdown',
    parentid: 'patients',
    child: [
      { path: `${BASEDIR}/branch`, name: 'All Branch' },
      { path: `${BASEDIR}/add-branch`, name: 'Add School Branch' }
    ]
  },
  { path: `${BASEDIR}/branch`, component: AllSchoolBranch, type: 'child' },
  { path: `${BASEDIR}/add-branch`, component: AddBranch, type: 'child' },
  { path: `${BASEDIR}/branch/edit/:id`, exact: true, component: EditBranch, type: 'child' },

  {
    path: '#',
    name: 'Variable Adjustments',
    icon: 'plus',
    type: 'dropdown',
    parentid: 'patients',
    child: [
      { path: `${BASEDIR}/adjustments`, name: 'All Adjustments Type' },
      { path: `${BASEDIR}/add-variable-adjustment`, name: 'Add Adjustment Type' }
    ]
  },
  { path: `${BASEDIR}/adjustments`, component: AllVariableAdjustment, type: 'child' },
  { path: `${BASEDIR}/add-variable-adjustment`, component: AddVariableType, type: 'child' },
  { path: `${BASEDIR}/adjustments/edit/:id`, component: EditVariableType, type: 'child' },

  {
    path: `${BASEDIR}/voucher`,
    name: 'Vouchers',
    icon: 'calendar',
    component: VoucherHome
  },
  { path: `${BASEDIR}/voucher/edit/:id`, component: Voucher, type: 'child' },
  { path: `${BASEDIR}/voucher/payslip/:id`, exact: true, component: Payslip, type: 'child' },
  { path: `${BASEDIR}/voucher/payslip/:id/edit`, component: EditPayslip, type: 'child' }
];
export default dashRoutes;
