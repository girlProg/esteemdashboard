// ##############################
// // // staffs
// #############################
var IMGDIR = process.env.REACT_APP_IMGDIR;

const staffs = [
    {avatar: IMGDIR+"/images/account/staffs/staff-1.jpg", name: "Evan Reid", position: "Nurse", age: "35", msg: "A account is a health care institution providing patient treatment with specialized medical and nursing staff and medical equipment."},
    {avatar: IMGDIR+"/images/account/staffs/staff-2.jpg", name: "David Roberts", position: "Cleaner", age: "35", msg: "The best-known type of account is the general account, which typically has an emergency department to treat urgent health problems "},
    {avatar: IMGDIR+"/images/account/staffs/staff-4.jpg", name: "Colin Vasar", position: "Driver", age: "35", msg: "A district account typically is the major health care facility in its region, with a large number of beds for intensive care"},
    {avatar: IMGDIR+"/images/account/staffs/staff-5.jpg", name: "Mary Randall", position: "Administrator", age: "35", msg: "Specialized accounts include trauma centers, rehabilitation accounts, children's accounts, seniors' (geriatric) accounts,"},
    {avatar: IMGDIR+"/images/account/staffs/staff-11.jpg", name: "Lily Wallace", position: "Pathologist", age: "35", msg: "A teaching account combines assistance to people with teaching to medical students and nurses."},
    {avatar: IMGDIR+"/images/account/staffs/staff-6.jpg", name: "Clarrie White", position: "Sr. Nurse", age: "35", msg: "The medical facility smaller than a account is generally called a clinic. accounts have a range of departments"},
    {avatar: IMGDIR+"/images/account/staffs/staff-8.jpg", name: "Nicola Smith", position: "Nurse", age: "35", msg: "Some accounts have outpatient departments and some have chronic treatment units. Common support units include a pharmacy."},
    {avatar: IMGDIR+"/images/account/staffs/staff-13.jpg", name: "Gordon Parr", position: "Cleaner", age: "35", msg: "accounts are usually funded by the public sector, health organisations (for profit or nonprofit), health insurance companies, or charities."},
]; 

export {
    staffs, 
};
