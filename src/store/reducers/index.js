import {combineReducers} from 'redux';
// import customerReducer from './customer';
import authReducer from './authReducer';
import staffReducer from './staffReducer';

export default combineReducers({
  // customers: customerReducer,
  authentication: authReducer,
  staffReducer : staffReducer,
})
