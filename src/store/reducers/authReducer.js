import { LOGIN, LOGIN_SUCCESS, LOGIN_START } from '../actions/constants';
// import { endpoints } from '../../services/esteemAPI';

const authReducer = (state = [], { type, payload }) => {
  switch (type) {
    case LOGIN:
      return { ...state, ...payload };
    case LOGIN_SUCCESS:
      return { ...state, ...payload };
    case LOGIN_START:
      return { ...state, ...payload };
    default:
      return state;
  }
};

// export const login = async () => async dispatch => {
//     const res = await endpoints.login('tima', 'Alpine162')
//     console.log (res)
//     return dispatch({type: LOGIN, payload: res})}

export default authReducer;
