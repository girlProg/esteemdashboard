import {
  GET_PAYSLIP,
  GET_PAYSLIP_SUCCESS,
  GET_STAFF,
  GET_PFAS_SUCCESS,
  GET_PFAS,
  ADD_PFA,
  ADD_PFA_SUCCESS,
  EDIT_PFA,
  EDIT_PFA_SUCCESS,
  DELETE_PFA,
  DELETE_PFA_SUCCESS,
  GET_BANKS_SUCCESS,
  GET_BANKS,
  ADD_BANK,
  ADD_BANK_SUCCESS,
  EDIT_BANK,
  EDIT_BANK_SUCCESS,
  DELETE_BANK,
  DELETE_BANK_SUCCESS,
  GET_SCHOOL_BRANCH_SUCCESS,
  GET_SCHOOL_BRANCH,
  ADD_SCHOOL_BRANCH,
  ADD_SCHOOL_BRANCH_SUCCESS,
  EDIT_SCHOOL_BRANCH,
  EDIT_SCHOOL_BRANCH_SUCCESS,
  DELETE_SCHOOL_BRANCH,
  DELETE_SCHOOL_BRANCH_SUCCESS,
  ADD_STAFF,
  ADD_STAFF_SUCCESS,
  ADD_BANK_ACCOUNT,
  ADD_BANK_ACCOUNT_SUCCESS,
  DELETE_STAFF_SUCCESS,
  DELETE_STAFF,
  FETCH_SUCCESS,
  FETCH_FAILURE,
  FETCH_PROCESS,
  EDIT_STAFF,
  EDIT_STAFF_SUCCESS,
  ADD_VARADJ,
  ADD_VARADJ_SUCCESS,
  EDIT_VARADJ,
  EDIT_VARADJ_SUCCESS,
  DELETE_VARADJ,
  DELETE_VARADJ_SUCCESS,
  GET_VARADJ,
  GET_VARADJ_SUCCESS,
  GET_VARADJ_TYPE,
  GET_VARADJ_TYPE_SUCCESS,
  ADD_VARADJ_TYPE,
  ADD_VARADJ_TYPE_SUCCESS,
  EDIT_VARADJ_TYPE,
  EDIT_VARADJ_TYPE_SUCCESS,
  DELETE_VARADJ_TYPE,
  DELETE_VARADJ_TYPE_SUCCESS
} from "../actions/constants";

const initialState = {
  loading: false,
  error: false,
  errorMesssage: null,
  status: null
};

const staffReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case GET_STAFF:
      return { ...state, ...payload };
    case ADD_STAFF:
      return { ...state, ...payload };
    case ADD_STAFF_SUCCESS:
      return { ...state, ...payload };
    case EDIT_STAFF:
      return { ...state, ...payload };
    case EDIT_STAFF_SUCCESS:
      return { ...state, ...payload };
    case DELETE_STAFF:
      return { ...state, ...payload };
    case DELETE_STAFF_SUCCESS:
      return { ...payload };
    case GET_PFAS:
      return { ...state, ...payload };
    case GET_PFAS_SUCCESS:
      return { ...state, ...payload };
    case ADD_PFA:
      return { ...state, ...payload };
    case ADD_PFA_SUCCESS:
      return { ...state, ...payload };
    case EDIT_PFA:
      return { ...state, ...payload };
    case EDIT_PFA_SUCCESS:
      return { ...state, ...payload };
    case DELETE_PFA:
      return { ...state, ...payload };
    case DELETE_PFA_SUCCESS:
      return { ...payload };
    case GET_PAYSLIP:
      return { ...state, ...payload };
    case GET_PAYSLIP_SUCCESS:
      return { ...state, ...payload };
    case GET_BANKS:
      return { ...state, ...payload };
    case GET_BANKS_SUCCESS:
      return { ...state, ...payload };
    case ADD_BANK:
      return { ...state, ...payload };
    case ADD_BANK_SUCCESS:
      return { ...state, ...payload };
    case EDIT_BANK:
      return { ...state, ...payload };
    case EDIT_BANK_SUCCESS:
      return { ...state, ...payload };
    case DELETE_BANK:
      return { ...state, ...payload };
    case DELETE_BANK_SUCCESS:
      return { ...payload };
    case GET_SCHOOL_BRANCH:
      return { ...state, ...payload };
    case GET_SCHOOL_BRANCH_SUCCESS:
      return { ...state, ...payload };
    case ADD_SCHOOL_BRANCH:
      return { ...state, ...payload };
    case ADD_SCHOOL_BRANCH_SUCCESS:
      return { ...state, ...payload };
    case EDIT_SCHOOL_BRANCH:
      return { ...state, ...payload };
    case EDIT_SCHOOL_BRANCH_SUCCESS:
      return { ...state, ...payload };
    case DELETE_SCHOOL_BRANCH:
      return { ...state, ...payload };
    case DELETE_SCHOOL_BRANCH_SUCCESS:
      return { ...payload };
    case ADD_BANK_ACCOUNT:
      return { ...state, ...payload };
    case ADD_BANK_ACCOUNT_SUCCESS:
      return { ...state, ...payload };
    case ADD_VARADJ:
      return { ...state, ...payload };
    case ADD_VARADJ_SUCCESS:
      return { ...state, ...payload };
    case EDIT_VARADJ:
      return { ...state, ...payload };
    case EDIT_VARADJ_SUCCESS:
      return { ...state, ...payload };
    case DELETE_VARADJ:
      return { ...state, ...payload };
    case DELETE_VARADJ_SUCCESS:
      return { ...payload };
    case GET_VARADJ:
      return { ...state, ...payload };
    case GET_VARADJ_SUCCESS:
      return { ...state, ...payload };
    case GET_VARADJ_TYPE:
      return { ...state, ...payload };
    case GET_VARADJ_TYPE_SUCCESS:
      return { ...state, ...payload };
    case ADD_VARADJ_TYPE:
      return { ...state, ...payload };
    case ADD_VARADJ_TYPE_SUCCESS:
      return { ...state, ...payload };
    case EDIT_VARADJ_TYPE:
      return { ...state, ...payload };
    case EDIT_VARADJ_TYPE_SUCCESS:
      return { ...state, ...payload };
    case DELETE_VARADJ_TYPE:
      return { ...state, ...payload };
    case DELETE_VARADJ_TYPE_SUCCESS:
      return { ...payload };

    case FETCH_PROCESS:
      return { ...state, ...payload };
    case FETCH_SUCCESS:
      return { ...state, ...payload };
    case FETCH_FAILURE:
      return { ...state, ...payload };
    default:
      return state;
  }
};

export default staffReducer;
