import {
  GET_PAYSLIP,
  GET_PAYSLIP_SUCCESS,
  GET_BANKS,
  GET_BANKS_SUCCESS,
  ADD_BANK,
  ADD_BANK_SUCCESS,
  EDIT_BANK,
  EDIT_BANK_SUCCESS,
  DELETE_BANK,
  DELETE_BANK_SUCCESS,
  GET_PFAS_SUCCESS,
  GET_PFAS,
  ADD_PFA,
  ADD_PFA_SUCCESS,
  EDIT_PFA,
  EDIT_PFA_SUCCESS,
  DELETE_PFA,
  DELETE_PFA_SUCCESS,
  GET_SCHOOL_BRANCH_SUCCESS,
  GET_SCHOOL_BRANCH,
  ADD_SCHOOL_BRANCH,
  ADD_SCHOOL_BRANCH_SUCCESS,
  EDIT_SCHOOL_BRANCH,
  EDIT_SCHOOL_BRANCH_SUCCESS,
  DELETE_SCHOOL_BRANCH,
  DELETE_SCHOOL_BRANCH_SUCCESS,
  ADD_STAFF,
  ADD_STAFF_SUCCESS,
  ADD_BANK_ACCOUNT,
  ADD_BANK_ACCOUNT_SUCCESS,
  DELETE_STAFF,
  DELETE_STAFF_SUCCESS,
  FETCH_SUCCESS,
  FETCH_PROCESS,
  FETCH_FAILURE,
  EDIT_STAFF_SUCCESS,
  EDIT_VARADJ,
  EDIT_VARADJ_SUCCESS,
  EDIT_STAFF,
  ADD_VARADJ,
  ADD_VARADJ_SUCCESS,
  DELETE_VARADJ,
  DELETE_VARADJ_SUCCESS,
  GET_VARADJ,
  GET_VARADJ_SUCCESS,
  GET_VARADJ_TYPE,
  GET_VARADJ_TYPE_SUCCESS,
  ADD_VARADJ_TYPE,
  ADD_VARADJ_TYPE_SUCCESS,
  EDIT_VARADJ_TYPE,
  EDIT_VARADJ_TYPE_SUCCESS,
  DELETE_VARADJ_TYPE,
  DELETE_VARADJ_TYPE_SUCCESS
} from './constants';
import { endpoints } from '../../services/esteemAPI';

function initialDispatch(dispatch, actionType, data) {
  dispatch({
    type: actionType,
    payload: { loading: true, error: false, errorMesssage: null, ...data }
  });
}

function onSuccess(dispatch, actionType, newObject, status = null) {
  dispatch({
    type: actionType,
    payload: {
      ...newObject,
      status,
      loading: false,
      error: false,
      errorMesssage: null
    }
  });
}

function onDeleteSuccess(dispatch, actionType, status) {
  dispatch({
    type: actionType,
    payload: {
      status,
      loading: false,
      error: false,
      errorMesssage: null
    }
  });
}

function onFailure(dispatch, actionType, errorMesssage, status = null) {
  dispatch({
    type: actionType,
    payload: { loading: false, error: true, errorMesssage, status }
  });
}

export const actions = {
  getAllStaff: async dispatch => {
    initialDispatch(dispatch, FETCH_PROCESS);
    const { rawResponse, content } = await endpoints.getAllStaff();

    if (rawResponse.ok) {
      onSuccess(dispatch, FETCH_SUCCESS, { staff: content });
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  editStaff: async (dispatch, staffObj) => {
    initialDispatch(dispatch, EDIT_STAFF);
    const { rawResponse } = await endpoints.editStaff(staffObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, EDIT_STAFF_SUCCESS, { staff: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  deleteStaff: async (dispatch, staffObj) => {
    initialDispatch(dispatch, DELETE_STAFF);
    const { rawResponse } = await endpoints.deleteStaff(staffObj);
    const { status, ok } = rawResponse;
    if (ok) {
      onDeleteSuccess(dispatch, DELETE_STAFF_SUCCESS, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, rawResponse, status);
    }
  },

  addStaff: async (dispatch, staffObj) => {
    initialDispatch(dispatch, ADD_STAFF);
    const { rawResponse } = await endpoints.addStaff(staffObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, ADD_STAFF_SUCCESS, { newstaff: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  getAllPFAs: async dispatch => {
    initialDispatch(dispatch, GET_PFAS);
    const { rawResponse, content } = await endpoints.getAllPFAs();

    if (rawResponse.ok) {
      onSuccess(dispatch, GET_PFAS_SUCCESS, { pfas: content });
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  addPfa: async (dispatch, pfaObj) => {
    initialDispatch(dispatch, ADD_PFA);
    const { rawResponse } = await endpoints.addPfa(pfaObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, ADD_PFA_SUCCESS, { pfa: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  editPfa: async (dispatch, pfaObj) => {
    initialDispatch(dispatch, EDIT_PFA);
    const { rawResponse } = await endpoints.editPfa(pfaObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, EDIT_PFA_SUCCESS, { pfa: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  deletePfa: async (dispatch, pfaObj) => {
    initialDispatch(dispatch, DELETE_PFA);
    const { rawResponse } = await endpoints.deletePfa(pfaObj);
    const { status, ok } = rawResponse;

    if (ok) {
      onDeleteSuccess(dispatch, DELETE_PFA_SUCCESS, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, rawResponse, status);
    }
  },

  getAllBanks: async dispatch => {
    initialDispatch(dispatch, GET_BANKS);
    const { rawResponse, content } = await endpoints.getAllBanks();

    if (rawResponse.ok) {
      onSuccess(dispatch, GET_BANKS_SUCCESS, { banks: content });
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  addBank: async (dispatch, bankObj) => {
    initialDispatch(dispatch, ADD_BANK);
    const { rawResponse } = await endpoints.addBank(bankObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, ADD_BANK_SUCCESS, { bank: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  editBank: async (dispatch, bankObj) => {
    initialDispatch(dispatch, EDIT_BANK);
    const { rawResponse } = await endpoints.editBank(bankObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, EDIT_BANK_SUCCESS, { bank: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  deleteBank: async (dispatch, bankObj) => {
    initialDispatch(dispatch, DELETE_BANK);
    const { rawResponse } = await endpoints.deleteBank(bankObj);
    const { status, ok } = rawResponse;

    if (ok) {
      onDeleteSuccess(dispatch, DELETE_BANK_SUCCESS, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, rawResponse, status);
    }
  },

  addBankAccount: async (dispatch, bankAccountObj) => {
    initialDispatch(dispatch, ADD_BANK_ACCOUNT);
    const { rawResponse } = await endpoints.addBankAccount(bankAccountObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, ADD_BANK_ACCOUNT_SUCCESS, { bankAccount: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  getAllPayslip: async dispatch => {
    initialDispatch(dispatch, GET_PAYSLIP);
    const { rawResponse, content } = await endpoints.getAllPayslip();

    if (rawResponse.ok) {
      onSuccess(dispatch, GET_PAYSLIP_SUCCESS, { allPayslip: content });
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  getPayslip: async (dispatch, month, year) => {
    initialDispatch(dispatch, GET_PAYSLIP, { payslip: {} });
    const { rawResponse, content } = await endpoints.getPayslip(month, year);

    if (rawResponse.ok) {
      onSuccess(dispatch, GET_PAYSLIP_SUCCESS, { payslip: content });
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  getAllSchoolBranch: async dispatch => {
    initialDispatch(dispatch, GET_SCHOOL_BRANCH);
    const { rawResponse, content } = await endpoints.getAllSchoolBranch();

    if (rawResponse.ok) {
      onSuccess(dispatch, GET_SCHOOL_BRANCH_SUCCESS, { allSchoolBranch: content });
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  addSchoolBranch: async (dispatch, schoolObj) => {
    initialDispatch(dispatch, ADD_SCHOOL_BRANCH);
    const { rawResponse } = await endpoints.addSchoolBranch(schoolObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, ADD_SCHOOL_BRANCH_SUCCESS, { schoolBranch: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  editSchoolBranch: async (dispatch, schoolObj) => {
    initialDispatch(dispatch, EDIT_SCHOOL_BRANCH);

    const { rawResponse } = await endpoints.editSchoolBranch(schoolObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, EDIT_SCHOOL_BRANCH_SUCCESS, { schoolBranch: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  deleteSchoolBranch: async (dispatch, schoolObj) => {
    initialDispatch(dispatch, DELETE_SCHOOL_BRANCH);
    const { rawResponse } = await endpoints.deleteSchoolBranch(schoolObj);
    const { status, ok } = rawResponse;

    if (ok) {
      onDeleteSuccess(dispatch, DELETE_SCHOOL_BRANCH_SUCCESS, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, rawResponse, status);
    }
  },

  getVarAdj: async (dispatch, month, year) => {
    initialDispatch(dispatch, GET_VARADJ);
    const { rawResponse, content } = await endpoints.getVarAdj(month, year);

    if (rawResponse.ok) {
      onSuccess(dispatch, GET_VARADJ_SUCCESS, { varAdj: content });
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  addVarAdj: async (dispatch, varObj) => {
    initialDispatch(dispatch, ADD_VARADJ);
    const { rawResponse } = await endpoints.addVarAdj(varObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, ADD_VARADJ_SUCCESS, { varAdj: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  editVarAdj: async (dispatch, varObj) => {
    initialDispatch(dispatch, EDIT_VARADJ);
    const { rawResponse } = await endpoints.editVarAdj(varObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, EDIT_VARADJ_SUCCESS, { varAdj: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  deleteVarAdj: async (dispatch, varObj) => {
    initialDispatch(dispatch, DELETE_VARADJ);
    const { rawResponse } = await endpoints.deleteVarAdj(varObj);
    const { status, ok } = rawResponse;

    if (ok) {
      onDeleteSuccess(dispatch, DELETE_VARADJ_SUCCESS, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, rawResponse, status);
    }
  },

  getVarAdjType: async dispatch => {
    initialDispatch(dispatch, GET_VARADJ_TYPE);
    const { rawResponse, content } = await endpoints.getVarAdjType();

    if (rawResponse.ok) {
      onSuccess(dispatch, GET_VARADJ_TYPE_SUCCESS, { allVarAdjType: content });
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  addVarAdjType: async (dispatch, varObj) => {
    initialDispatch(dispatch, ADD_VARADJ_TYPE);
    const { rawResponse } = await endpoints.addVarAdjType(varObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, ADD_VARADJ_TYPE_SUCCESS, { varAdjType: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  editVarAdjType: async (dispatch, varObj) => {
    initialDispatch(dispatch, EDIT_VARADJ_TYPE);
    const { rawResponse } = await endpoints.editVarAdjType(varObj);
    const content = await rawResponse.json();
    const { status, ok } = rawResponse;

    if (ok) {
      onSuccess(dispatch, EDIT_VARADJ_TYPE_SUCCESS, { varAdjType: content }, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, content);
    }
  },

  deleteVarAdjType: async (dispatch, varObj) => {
    initialDispatch(dispatch, DELETE_VARADJ_TYPE);
    const { rawResponse } = await endpoints.deleteVarAdjType(varObj);
    const { status, ok } = rawResponse;

    if (ok) {
      onDeleteSuccess(dispatch, DELETE_VARADJ_TYPE_SUCCESS, status);
    } else {
      onFailure(dispatch, FETCH_FAILURE, rawResponse, status);
    }
  }
};
