import { LOGIN_SUCCESS, LOGIN_START as LOGIN_PROCESS } from './constants';
import { endpoints } from '../../services/esteemAPI';

export const actions = {
  // login :  async (dispatch, username='tima', password='Alpine162') => {
  login: async (dispatch, username, password) => {
    dispatch({ type: LOGIN_PROCESS, payload: { loading: true, error: false, errorMesssage: null } });
    const { rawResponse } = await endpoints.login(username, password);
    const content = await rawResponse.json();
    if (rawResponse.ok) {
      localStorage.setItem('token', content.auth_token);
      dispatch({
        type: LOGIN_SUCCESS,
        payload: { token: content.auth_token, error: false, errorMesssage: null, loading: false }
      });
    } else {
      dispatch({ type: LOGIN_PROCESS, payload: { error: true, errorMesssage: content, loading: false } });
    }
  },

  signup: async (dispatch, username, password) => {
    dispatch({ type: LOGIN_PROCESS, payload: { loading: true, error: false, errorMesssage: null } });
    const { rawResponse, content } = await endpoints.signup(username, password);
    // console.log(content)
    if (rawResponse.ok) {
      dispatch({ type: LOGIN_SUCCESS, payload: { user: content, error: false, errorMesssage: null, loading: false } });
    } else {
      dispatch({ type: LOGIN_PROCESS, payload: { error: true, errorMesssage: content, loading: false } });
    }
  }
};
