import React, { useEffect, useRef } from 'react';
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from 'perfect-scrollbar';
import { Route, Switch, Redirect, useHistory } from 'react-router-dom';
import { Header, Footer, Sidebar } from 'components';

import dashRoutes from 'routes/account';
import { topbarStyle, menuStyle, menuType, topbarType, navWidth } from 'variables/settings/account';

let ps;

export default function AccountLayout(props) {
  const mainPanelRef = useRef(null);
  const themeWrapperRef = useRef(null);
  const history = useHistory();
  const state = {
    menuColor: menuStyle,
    topbarColor: topbarStyle,
    menuType,
    topbarType
  };

  useEffect(() => {
    if (navigator.platform.indexOf('Win') > -1) {
      ps = new PerfectScrollbar(mainPanelRef.current);
      document.body.classList.toggle('perfect-scrollbar-on');
    }

    if (history.action === 'PUSH') {
      mainPanelRef.current.scrollTop = 0;
      document.scrollingElement.scrollTop = 0;
    }

    return () => {
      if (navigator.platform.indexOf('Win') > -1) {
        ps.destroy();
        document.body.classList.toggle('perfect-scrollbar-on');
      }
    };
  }, [history.action]);

  return (
    <div
      className="wrapper"
      ref={themeWrapperRef}
      data-menu={state.menuColor}
      data-topbar={state.topbarColor}
      data-menutype={state.menuType}
      data-topbartype={state.topbarType}
    >
      <Header {...props} navtype={navWidth} admintype="account" />
      <Sidebar {...props} routes={dashRoutes} admintype="account" />
      <div className="main-panel" ref={mainPanelRef}>
        <Switch>
          {dashRoutes.map((prop, key) => {
            if (props.location.pathname === prop.path) {
              return <Route path={`${prop.path}`} component={prop.component} key={key} />;
            }

            if (prop.path.includes(':')) {
              // const index = prop.path.indexOf(':');
              // const slug = prop.path.slice(index - 1);
              return <Route exact={prop.exact} path={`${prop.path}`} component={prop.component} key={key} />;
            }
            return '';
          })}
        </Switch>
        <Footer fluid />
      </div>
      {!localStorage.getItem('token') && <Redirect to="/esteem/login"></Redirect>}
    </div>
  );
}
