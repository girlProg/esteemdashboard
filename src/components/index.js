/* --------------- Common Components ------------------*/
import Header from './common/Header/Header';
import Footer from './common/Footer/Footer';
import Sidebar from './common/Sidebar/Sidebar';

/* --------------- account Admin Components ------------------*/
// import Doctorslist from './account/Doctorslist/Doctorslist';
// import Patientslist from './account/Patientslist/Patientslist';
// import Stafflist from './account/Stafflist/Stafflist';

/* --------------- Unused Components ------------------*/
// import PanelHeader from './general/PanelHeader/PanelHeader';
// import Stats from './general/Stats/Stats';
// import Tasks from './general/Tasks/Tasks';

export {
  Header,
  Footer,
  Sidebar
  // //
  // Doctorslist,
  // Patientslist
  // Stafflist
};
