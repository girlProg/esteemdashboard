import React, { useState, useEffect, useRef } from 'react';
import { NavLink } from 'react-router-dom';
import { Nav } from 'reactstrap';
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from 'perfect-scrollbar';
import logofull from 'assets/img/logo-full.png';
import logomini from 'assets/img/logo-mini.png';
import logofulldark from 'assets/img/logo-full-dark.png';
import logominidark from 'assets/img/logo-mini-dark.png';

let ps;
let currentmenu = 'notset';

const IMGDIR = process.env.REACT_APP_IMGDIR;

export default function Sidebar({ location, admintype, routes }) {
  const sidebarRef = useRef(null);
  const [state, setState] = useState({
    opendd: '',
    openmenu: 'none',
    profilename: 'Staff Name',
    profileposition: 'Accounts',
    profileimg: `${IMGDIR}/images/profile/profile-account.jpg`
  });

  // const handlecurrent = currentmenu => {
  //   if (state.opendd !== '') currentmenu = '';
  // };

  const handleOpendd = open => {
    currentmenu = '';
    if (state.opendd === open) {
      setState(prev => ({ ...prev, opendd: '' }));
    } else {
      setState(prev => ({ ...prev, opendd: open }));
    }
  };

  // verifies if routeName is the one active (in browser input)
  const activeRoute = routeName => (location.pathname.indexOf(routeName) > -1 ? ' active' : '');

  useEffect(() => {
    if (navigator.platform.indexOf('Win') > -1) {
      ps = new PerfectScrollbar(sidebarRef.current, { suppressScrollX: true, suppressScrollY: false });
    }

    return () => {
      if (navigator.platform.indexOf('Win') > -1) {
        ps.destroy();
      }
    };
  }, []);

  const children = (child, parent) => {
    const links = [];
    if (child) {
      for (let i = 0; i < child.length; i++) {
        links.push(
          <li key={i}>
            <NavLink to={child[i].path} className="nav-link" activeClassName="active">
              <span>{child[i].name}</span>
            </NavLink>
          </li>
        );
        // console.log(child[i].parent + this.props.location.pathname + child[i].path);
        if (location.pathname === child[i].path) {
          // console.log("match found " + child[i].parent);
          if (currentmenu === 'notset' && state.opendd === '') {
            currentmenu = parent; // child[i].parent;
          }
        }
        if (location.pathname === '/') {
          currentmenu = 'dashboards';
        }
      }
      return <Nav>{links}</Nav>;
    }
  };

  return (
    <div className="sidebar menubar" data-color="black">
      <div className="logo">
        <a href="/" className="logo-mini">
          <div className="logo-img">
            <img src={logomini} alt="react-logo" className="light-logo" />
            <img src={logominidark} alt="react-logo" className="dark-logo" />
          </div>
        </a>
        <a href="/" className="logo-full">
          <img src={logofull} alt="react-logo" className="light-logo" />
          <img src={logofulldark} alt="react-logo" className="dark-logo" />
        </a>
      </div>

      <div className="sidebar-wrapper" ref={sidebarRef}>
        <div className="profile-info row">
          <div className="profile-image col-4">
            <a href="#!">
              <img alt="" src={state.profileimg} className="img-fluid avatar-image" />
            </a>
          </div>
          <div className="profile-details col-8">
            <h3>
              <a href="#!">{state.profilename}</a>
              <span className="profile-status online"></span>
            </h3>
            <p className="profile-title">{state.profileposition}</p>
          </div>
        </div>

        <Nav className="navigation">
          {routes.map((prop, key) => {
            if (prop.redirect) return null;
            if (prop.type === 'child') return null;
            //   if (prop.type === 'navgroup') return <Navmenugroup name={prop.name} key={key}></Navmenugroup>;
            if (prop.type === 'dropdown') {
              return (
                <li
                  className={`${prop.parentid} ${
                    (prop.parentid === currentmenu && prop.parentid !== '' && prop.parentid !== 'multipurpose') ||
                    state.opendd === prop.name
                      ? 'active'
                      : ''
                  } nav-parent `}
                  data-toggle="collapse"
                  key={key}
                >
                  <a to={prop.path} className="nav-link" onClick={() => handleOpendd(prop.name)} href="#!">
                    <i className={`i-${prop.icon}`}></i>
                    <p>{prop.name}</p>
                    <span className="badge">{prop.badge}</span>
                    <span className="arrow i-arrow-left"></span>
                  </a>
                  {children(prop.child, prop.parentid)}
                </li>
              );
            }
            return (
              <li className={`${activeRoute(prop.path)} nav-parent `} key={key} onClick={() => handleOpendd(prop.name)}>
                <NavLink to={prop.path} className="nav-link" activeClassName="active">
                  <i className={`i-${prop.icon}`}></i>
                  <p>{prop.name}</p>
                  <span className="badge">{prop.badge}</span>
                </NavLink>
              </li>
            );
          })}
        </Nav>
      </div>
    </div>
  );
}
