import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import {
  Collapse,
  Navbar,
  NavbarBrand,
  Nav,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container
} from 'reactstrap';

import dashboardRoutes from 'routes/account.jsx';

const IMGDIR = process.env.REACT_APP_IMGDIR;
function Header({ location, navtype, admintype, history }) {
  const sidebarToggle = useRef(null);
  const [state, setState] = useState({
    isOpen: false,
    userddOpen: false,
    color: 'primary',
    profilename: '',
    profileimg: ''
  });

  const userddToggle = () => {
    setState(prev => ({ ...prev, userddOpen: !prev.userddOpen }));
  };

  const getBrand = () => {
    let name;
    dashboardRoutes.map((prop, key) => {
      if (prop.collapse) {
        prop.views.map((prop, key) => {
          if (prop.path === location.pathname) {
            name = prop.name;
          }
          return null;
        });
      } else if (prop.redirect) {
        if (prop.path === location.pathname) {
          name = prop.name;
        }
      } else if (prop.path === location.pathname) {
        name = prop.name;
      }
      return null;
    });
    return name;
  };

  const openSidebar = () => {
    document.documentElement.classList.toggle('nav-toggle');
    if (sidebarToggle.current) sidebarToggle.current.classList.toggle('toggled');
  };

  const toggleGrid = () => {
    document.documentElement.classList.toggle('toggle-grid');
  };

  useEffect(() => {
    if (navtype === 'mini') {
      document.documentElement.classList.add('nav-toggle');
      sidebarToggle.current.classList.add('toggled');
    } else {
      document.documentElement.classList.remove('nav-toggle');
      sidebarToggle.current.classList.remove('toggled');
    }

    if (admintype === 'account') {
      setState(prev => ({
        ...prev,
        profilename: 'Staff Name',
        profileimg: `${IMGDIR}/images/profile/profile-account.jpg`
      }));
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (
      window.innerWidth < 993 &&
      history.location.pathname !== location.pathname &&
      document.documentElement.className.indexOf('nav-toggle') !== -1
    ) {
      document.documentElement.classList.toggle('nav-toggle');
      sidebarToggle.current.classList.toggle('toggled');
    }
    if (
      window.innerWidth < 993 &&
      history.location.pathname !== location.pathname &&
      document.documentElement.className.indexOf('nav-toggle-chat') !== -1
    ) {
      document.documentElement.classList.toggle('nav-toggle-chat');
      // this.refs.chatToggle.classList.toggle('toggled');
    }
  }, [history, location]);

  return (
    // add or remove classes depending if we are on full-screen-maps page or not
    <Navbar
      expand="lg"
      className={
        location.pathname.indexOf('full-screen-maps') !== -1
          ? 'navbar-absolute fixed-top'
          : 'navbar-absolute fixed-top '
      }
    >
      <Container fluid>
        <div className="navbar-wrapper">
          <div className="navbar-toggle">
            <button type="button" ref={sidebarToggle} className="navbar-toggler" onClick={() => openSidebar()}>
              <i className="i-menu"></i>
            </button>
          </div>

          <NavbarBrand href="/">{getBrand()}</NavbarBrand>
        </div>

        <Collapse isOpen={state.isOpen} navbar className="navbar-right">
          <Nav navbar>
            <Dropdown nav isOpen={state.userddOpen} toggle={e => userddToggle(e)} className="userdd">
              <DropdownToggle caret nav>
                <img src={state.profileimg} alt="react-logo" className="avatar-image" />{' '}
                <span>{state.profilename}</span>
              </DropdownToggle>
              <DropdownMenu right>
                {/* <DropdownItem tag="a">
                    <i className="i-wrench" href="#!"></i> Settings
                  </DropdownItem>
                  <DropdownItem tag="a">
                    <i className="i-user" href="#!"></i> Profile
                  </DropdownItem>
                  <DropdownItem tag="a">
                    <i className="i-info" href="#!"></i> Help
                  </DropdownItem> */}
                <DropdownItem
                  tag="a"
                  className=""
                  href="#!"
                  onClick={() => {
                    localStorage.removeItem('token');
                    window.location.reload();
                  }}
                >
                  <i className="i-lock"></i> Logout
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </Nav>
          <div className="screensize" onClick={() => toggleGrid()}></div>
        </Collapse>
      </Container>
    </Navbar>
  );
}

export default Header;

Header.propTypes = {
  location: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
  navtype: PropTypes.string.isRequired,
  admintype: PropTypes.string.isRequired
};
