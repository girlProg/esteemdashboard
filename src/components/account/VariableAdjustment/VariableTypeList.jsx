import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button } from 'reactstrap';
import Loading from '../Utils/Loading';
import ButtonWithPopOver from '../ButtonWithPopOver';

export default function VariableTypeList({ getSelectedAdj, allVarAdjType, handleEdit, handleDelete, loading }) {
  return (
    <div>
      <div className="content">
        <Row>
          {loading ? (
            <Loading />
          ) : (
            <Col xs={12} md={12}>
              <div className="page-title">
                <div className="float-left">
                  <h1 className="title">Variable Adjustment Type</h1>
                </div>
              </div>

              <div className="col-xl-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title float-left">All Variable Adjustments</h2>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      <div className="col-12 d-flex flex-wrap">
                        {allVarAdjType?.results.map(adj => (
                          <div className="col-md-6 col-lg-6 col-sm-12" key={adj.id}>
                            <div className="team-member w-100 h-80 pfa-card">
                              <div className="row margin-0">
                                <div className="team-info">
                                  <h3 className="name">{adj.name}</h3>
                                  <div>
                                    <Button color="warning" size="sm" onClick={handleEdit}>
                                      EDIT
                                    </Button>
                                    <ButtonWithPopOver
                                      target={adj.id}
                                      getSelected={getSelectedAdj}
                                      handleDelete={handleDelete}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </Col>
          )}
        </Row>
      </div>
    </div>
  );
}

VariableTypeList.propTypes = {
  getSelectedAdj: PropTypes.func.isRequired,
  allVarAdjType: PropTypes.object,
  handleEdit: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
};
