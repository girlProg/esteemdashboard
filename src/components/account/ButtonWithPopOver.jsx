import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Button, Popover, PopoverHeader, PopoverBody } from 'reactstrap';

export default function ButtonWithPopOver({ size, target, getSelected, handleDelete }) {
  const popOverId = `target-${target}`;
  const [popoverOpen, setPopoverOpen] = useState(false);
  const togglePopOver = () => setPopoverOpen(!popoverOpen);

  return (
    <>
      <Button
        color="danger"
        id={popOverId}
        size={size || 'sm'}
        onClick={e => {
          togglePopOver();
          if (getSelected) getSelected(e);
        }}
      >
        DELETE
      </Button>
      <Popover placement="right" isOpen={popoverOpen} target={popOverId} toggle={togglePopOver}>
        <PopoverHeader>
          <span style={{ color: 'red' }}>This action cannot be reversed. Continue?</span>
        </PopoverHeader>
        <PopoverBody>
          <Button size="sm" onClick={togglePopOver}>
            No
          </Button>
          <Button size="sm" color="danger" onClick={handleDelete}>
            YES
          </Button>
        </PopoverBody>
      </Popover>
    </>
  );
}

ButtonWithPopOver.propTypes = {
  size: PropTypes.string,
  target: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  getSelected: PropTypes.func,
  handleDelete: PropTypes.func.isRequired
};
