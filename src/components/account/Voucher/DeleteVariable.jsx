import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Label } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import ButtonWithPopOver from '../ButtonWithPopOver';
import Loading from '../Utils/Loading';

export default function DeleteVariable({ slip, varAdj }) {
  const history = useHistory();
  const dispatch = useDispatch();
  const { status, loading } = useSelector(state => state.staffReducer);
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const [state, setState] = useState({
    id: '',
    name: ''
  });
  const slipAdjList = [];
  let selectedAdj;

  if (varAdj?.results) {
    varAdj.results.forEach(adj => {
      if (adj.payslip === slip.id) {
        slipAdjList.push(adj);
      }
    });
  }

  useEffect(() => {
    if (status === 204) {
      setState({
        id: '',
        name: ''
      });
      setTimeout(() => history.go(0), 1500);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  const toggle = () => setDropdownOpen(prevState => !prevState);

  slipAdjList.forEach(adj => {
    if (adj.id === Number(state.id)) {
      selectedAdj = adj;
    }
  });

  const dropDownSelect = e => {
    const { innerText, value } = e.target;
    setState({
      id: value,
      name: innerText
    });
  };

  const handleDelete = async e => {
    e.preventDefault();
    const varObj = {
      id: selectedAdj.id,
      name: { id: selectedAdj.name.id },
      payslip: selectedAdj.payslip
    };

    await actions.deleteVarAdj(dispatch, varObj);
  };

  return (
    <form>
      {loading ? (
        <Loading width="50" top="20%" />
      ) : (
        <>
          {slipAdjList.length > 0 ? (
            <>
              <div className="form-row">
                <div className="form-group col-md-6">
                  <Label htmlFor="variableNname">Variable Name</Label>
                  <Dropdown className="d-block ml-0" isOpen={dropdownOpen} toggle={toggle}>
                    <DropdownToggle color={state.name ? 'primary' : 'secondary'} caret>
                      {state.name || 'Select an option...'}
                    </DropdownToggle>
                    <DropdownMenu>
                      {slipAdjList?.map(variable => (
                        <div key={variable.id}>
                          <DropdownItem value={variable.id} onClick={dropDownSelect}>
                            {variable.name.name}
                          </DropdownItem>
                          <DropdownItem divider></DropdownItem>
                        </div>
                      ))}
                    </DropdownMenu>
                  </Dropdown>
                </div>
              </div>
              <div>
                <ButtonWithPopOver size="lg" target="popOver" handleDelete={handleDelete} />
              </div>
            </>
          ) : (
            <h5 className="text-center">There are no variable adjustments for this staff.</h5>
          )}
        </>
      )}
    </form>
  );
}

DeleteVariable.propTypes = {
  slip: PropTypes.object,
  varAdj: PropTypes.object
};
