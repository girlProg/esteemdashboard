import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Input, Label, DropdownToggle, DropdownMenu, DropdownItem, Dropdown } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import Loading from '../Utils/Loading';

export default function EditVariable({ slip, varAdj }) {
  const history = useHistory();
  const dispatch = useDispatch();
  const { status, loading } = useSelector(state => state.staffReducer);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [dropdownOpen1, setDropdownOpen1] = useState(false);
  let selectedAdj;
  const slipAdjList = [];

  const [state, setState] = useState({
    id: '',
    name: '',
    type: 'Select an option...',
    amount: ''
  });

  useEffect(() => {
    if (status === 200) {
      setState({
        id: '',
        name: '',
        type: 'Select an option...',
        amount: ''
      });
      setTimeout(() => history.go(0), 1500);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  if (varAdj) {
    varAdj.results &&
      varAdj.results.forEach(adj => {
        adj.payslip === slip.id && slipAdjList.push(adj);
      });
  }

  const toggle = () => setDropdownOpen(prevState => !prevState);
  const toggle1 = () => setDropdownOpen1(prevState => !prevState);

  const varDropDownSelect = e => {
    const { innerText, value } = e.target;
    setState(() => ({
      ...state,
      id: value,
      name: innerText
    }));
  };

  const typeDropDownSelect = e => {
    const { name, value } = e.target;
    setState(() => ({
      ...state,
      [name]: value
    }));
  };

  const handleAmountChange = e => {
    const { name, value } = e.target;
    setState({ ...state, [name]: value });
  };

  const handleDecimal = e => {
    const { name, value } = e.target;
    let num = parseFloat(value);
    if (value === '') {
      num = 0;
    }
    const numToTwodecimal = num.toFixed(2);
    setState({ ...state, [name]: numToTwodecimal });
  };

  slipAdjList.forEach(adj => {
    if (adj.id === Number(state.id)) {
      selectedAdj = adj;
    }
  });

  const handleSubmit = async e => {
    e.preventDefault();
    const varObj = {
      id: selectedAdj.id,
      name: { id: selectedAdj.name.id },
      type: state.type,
      amount: state.amount,
      payslip: selectedAdj.payslip
    };

    await actions.editVarAdj(dispatch, varObj);
  };

  return (
    <form onSubmit={handleSubmit}>
      {loading ? (
        <Loading width="50" top="20%" />
      ) : (
        <>
          {slipAdjList.length > 0 ? (
            <>
              <div className="form-row">
                <div className="form-group col-md-12">
                  <Label htmlFor="edit_variable_name">Variable Name</Label>
                  <Dropdown className="d-block ml-0" isOpen={dropdownOpen} toggle={toggle}>
                    <DropdownToggle color={state.name ? 'primary' : 'secondary'} caret>
                      {state.name || 'Select an option...'}
                    </DropdownToggle>
                    <DropdownMenu>
                      <></>
                      {slipAdjList.map(variable => (
                        <div key={variable.id}>
                          <DropdownItem name="id" value={variable.id} onClick={varDropDownSelect}>
                            {variable.name.name}
                          </DropdownItem>
                          <DropdownItem divider></DropdownItem>
                        </div>
                      ))}
                    </DropdownMenu>
                  </Dropdown>
                </div>
                <div className="form-group col-md-12">
                  <Label htmlFor="edit_type">Type</Label>
                  <Dropdown className="d-block ml-0" isOpen={dropdownOpen1} toggle={toggle1}>
                    <DropdownToggle color={state.type === 'Select an option...' ? 'secondary' : 'primary'} caret>
                      {state.type === 'Positive' && 'Increment'}
                      {state.type === 'Negative' && 'Decrement'}
                    </DropdownToggle>
                    <DropdownMenu>
                      <DropdownItem name="type" value="Positive" onClick={typeDropDownSelect}>
                        Increment
                      </DropdownItem>
                      <DropdownItem divider></DropdownItem>
                      <DropdownItem name="type" value="Negative" onClick={typeDropDownSelect}>
                        Decrement
                      </DropdownItem>
                    </DropdownMenu>
                  </Dropdown>
                </div>
                <div className="form-group col-md-6">
                  <Label htmlFor="edit_amount">Amount</Label>
                  <Input
                    type="number"
                    name="amount"
                    value={state.amount}
                    id="edit_amount"
                    onChange={handleAmountChange}
                    onBlur={handleDecimal}
                  ></Input>
                </div>
              </div>
              <button
                type="submit"
                disabled={!state.name || !state.type || !state.amount}
                className="my-4 btn btn-primary"
              >
                UPDATE VARIABLE
              </button>
            </>
          ) : (
            <h5 className="text-center">There are no variable adjustments for this staff.</h5>
          )}
        </>
      )}
    </form>
  );
}

EditVariable.propTypes = {
  slip: PropTypes.object,
  varAdj: PropTypes.object
};
