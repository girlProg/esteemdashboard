import React from 'react';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { Button } from 'reactstrap';
import moment from 'moment';
import PropTypes from 'prop-types';
import logo from '../../../assets/img/logo-color.png';
import { formatNumber } from '../Utils/format';

const GeneratePDF = ({ body, head, slip, config, month, year, allPayslips, varAdjArr, text }) => {
  // eslint-disable-next-line prettier/prettier
  const vMonth = moment(`${year}-${month}`).format('MMMM').toUpperCase();
  let sn = 0;

  const imgData = new Image();
  imgData.src = logo;
  const position = {
    width: 85,
    height: 90,
    left: 250,
    top: 30
  };

  const exportPDF = {
    voucher: () => {
      const doc = new jsPDF('l', 'pt', 'A1');
      const header = head.map(title => [title.title]);
      const newHeader = header.filter(h => h[0] !== 'S/N' && h[0] !== 'View' && h[0] !== 'Edit');

      const data = body.map(el => {
        const staffData = [
          (sn += 1),
          el.name,
          el.basic,
          el.housing,
          el.transport,
          el.meal,
          el.utility,
          el.education,
          el.dressing,
          el.medical,
          el.salaryAmount,
          el.allowanceforHeads,
          el.grossIncome,
          el.pension,
          el.nhis,
          el.tax,
          el.credittobank
        ];
        varAdjArr.forEach(adj => {
          if (el[adj]) staffData.push(el[adj]);
        });
        return staffData;
      });

      doc.addImage(imgData, 'png', 25, 10, 50, 50);

      doc.setFontSize(14);
      doc.text(`Esteem International School Monthly Voucher for ${vMonth}, ${year}`, 220, 45);

      doc.autoTable({
        head: [['S/N', ...newHeader]],
        body: data,
        startY: 70,
        margin: { left: 10 },
        styles: {
          overflow: 'hidden',
          cellWidth: 'wrap',
          fontSize: 4.3
        },
        showHead: 'firstPage',
        willDrawCell(data) {
          if (data.row.section === 'body') {
            if (data.row.raw[1] === 'TOTAL') {
              doc.setFontStyle('bold');
            }
          }
        }
      });

      doc.save(`voucher-for-${vMonth}-${year}.pdf`);
    },

    payslip: () => {
      const slipMonth = moment(`${slip.year}-${slip.month}`)
        .format('MMMM')
        .toUpperCase();
      const doc = new jsPDF('p', 'pt', 'A4');

      const header = head.map(title => [title.Header]);

      const data = body.map(el => [(sn += 1), el.description, el.amount, el.constant]);

      doc.addImage(imgData, 'png', position.left, position.top, position.width, position.height);

      doc.setFontSize(13);
      doc.text(`Esteem International School Monthly Payslip for ${slipMonth}, ${slip.year}`, 120, 150 /* marginTop */);

      const { pageSize, getFontSize, scaleFactor } = doc.internal;
      const pageWidth = pageSize.width ? pageSize.width : pageSize.getWidth();
      const text = `Staff name:  ${slip.staff.name}`;
      const txtWidth = (doc.getStringUnitWidth(text) * getFontSize()) / scaleFactor;
      const xOffset = (pageWidth - txtWidth) / 2;

      doc.text(text, xOffset, 170);

      doc.autoTable({
        head: [['S/N', ...header]],
        body: data,
        startY: 190,
        theme: 'grid',
        margin: { left: 35 },
        styles: {
          overflow: 'ellipsize',
          cellWidth: 'wrap',
          fontSize: 12
        },
        showHead: 'firstPage',
        willDrawCell(data) {
          if (data.row.section === 'body') {
            if (data.row.raw[1] === 'Credit to Bank') {
              doc.setFontStyle('bold');
            }
            if (data.row.index > 13 && data.row.raw[1] !== 'Credit to Bank') doc.setFontStyle('italic');
          }
        }
      });

      doc.save(`payslip-${slip.staff.name}-${slipMonth}-${slip.year}.pdf`);
    },

    bankReport: () => {
      const doc = new jsPDF('p', 'pt', 'A4');

      // calculate sum of all amount
      const filterBody = body.slice(0, body.length - 1);
      const allCredit = filterBody.map(item => Number(item.credittobank.replace(',', '')));
      const total = allCredit.reduce((sum, num) => sum + num).toFixed(2);

      const slips = [
        ...filterBody,
        {
          name: 'TOTAL',
          bankAccount: [],
          credittobank: formatNumber(total)
        }
      ];

      const slipsData = slips.map(el => [
        el.name !== 'TOTAL' ? (sn += 1) : '',
        el.name,
        el.bankAccount,
        el.credittobank
      ]);

      doc.addImage(imgData, 'png', position.left, position.top, position.width, position.height);
      doc.setFontSize(14);
      doc.text(`Esteem International School Monthly Bank Report for ${vMonth}, ${year}`, 80, 150 /* marginTop */);
      doc.autoTable({
        head: [['S/N', 'Name', 'Account Number', 'Credit to Bank']],
        body: slipsData,
        startY: 170,
        theme: 'grid',
        margin: { left: 30 },
        styles: {
          overflow: 'ellipsize',
          cellWidth: 'wrap',
          fontSize: 11
        },
        showHead: 'firstPage',
        willDrawCell(data) {
          if (data.row.section === 'body') {
            if (data.row.raw[1] === 'TOTAL') {
              doc.setFontStyle('bold');
            }
          }
        }
      });

      doc.save(`bank-report-for-${vMonth}-${year}.pdf`);
    }
  };

  return (
    <Button
      onClick={() => {
        if (config === 'voucher') return exportPDF.voucher();
        if (config === 'bank') return exportPDF.bankReport();
        return exportPDF.payslip();
      }}
      color="danger"
    >
      {text}
    </Button>
  );
};

export default GeneratePDF;
