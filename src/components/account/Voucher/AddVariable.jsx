import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Input, Label, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import Loading from '../Utils/Loading';

export default function AddVariable({ slip, allVarAdjType }) {
  const history = useHistory();
  const dispatch = useDispatch();
  const { status, loading } = useSelector(state => state.staffReducer);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const [dropdownOpen1, setDropdownOpen1] = useState(false);

  const [state, setState] = useState({
    id: '',
    name: '',
    type: 'Select an option...',
    amount: ''
  });

  const handleChange = e => {
    const { name, value } = e.target;
    setState({ ...state, [name]: value });
  };

  const handleDecimal = e => {
    const { name, value } = e.target;
    let num = parseFloat(value);
    if (value === '') {
      num = 0;
    }
    const numToTwodecimal = num.toFixed(2);
    setState({ ...state, [name]: numToTwodecimal });
  };

  const toggle = () => setDropdownOpen(prevState => !prevState);
  const toggle1 = () => setDropdownOpen1(prevState => !prevState);

  const varDropDownSelect = e => {
    const { innerText, value } = e.target;
    setState(() => ({
      ...state,
      id: value,
      name: innerText
    }));
  };

  const amountDropDownSelect = e => {
    const { name, value } = e.target;
    setState(() => ({
      ...state,
      [name]: value
    }));
  };

  useEffect(() => {
    if (status === 201) {
      setState({
        id: '',
        name: '',
        type: 'Select an option...',
        amount: ''
      });
      setTimeout(() => history.go(0), 1500);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  const handleSubmit = async e => {
    e.preventDefault();
    const varObj = {
      name: { id: state.id },
      type: state.type,
      amount: state.amount,
      payslip: slip.id
    };

    await actions.addVarAdj(dispatch, varObj);
  };

  return (
    <form onSubmit={handleSubmit}>
      {loading ? (
        <Loading width="50" top="20%" />
      ) : (
        <>
          <div className="form-row">
            <div className="form-group col-md-12">
              <Label htmlFor="variable_name">Variable Name</Label>
              <Dropdown className="d-block ml-0" isOpen={dropdownOpen} toggle={toggle}>
                <DropdownToggle color={state.name ? 'primary' : 'secondary'} caret>
                  {state.name || 'Select an option...'}
                </DropdownToggle>
                <DropdownMenu>
                  {allVarAdjType &&
                    allVarAdjType.results.map(variable => (
                      <div key={variable.id}>
                        <DropdownItem name="id" value={variable.id} onClick={varDropDownSelect}>
                          {variable.name}
                        </DropdownItem>
                        <DropdownItem divider></DropdownItem>
                      </div>
                    ))}
                </DropdownMenu>
              </Dropdown>
            </div>
            <div className="form-group col-md-12">
              <Label htmlFor="type">Type</Label>
              <Dropdown className="d-block ml-0" isOpen={dropdownOpen1} toggle={toggle1}>
                <DropdownToggle color={state.type === 'Select an option...' ? 'secondary' : 'primary'} caret>
                  {state.type === 'Positive' ? 'Increment' : state.type === 'Negative' ? 'Decrement' : state.type}
                </DropdownToggle>
                <DropdownMenu>
                  <DropdownItem name="type" value="Positive" onClick={amountDropDownSelect}>
                    Increment
                  </DropdownItem>
                  <DropdownItem divider></DropdownItem>
                  <DropdownItem name="type" value="Negative" onClick={amountDropDownSelect}>
                    Decrement
                  </DropdownItem>
                </DropdownMenu>
              </Dropdown>
            </div>
            <div className="form-group col-md-6">
              <Label htmlFor="amount">Amount</Label>
              <Input
                type="number"
                name="amount"
                value={state.amount}
                id="amount"
                onChange={handleChange}
                onBlur={handleDecimal}
              ></Input>
            </div>
          </div>
          <button type="submit" disabled={!state.id || !state.type || !state.amount} className="btn btn-primary my-4">
            ADD VARIABLE
          </button>
        </>
      )}
    </form>
  );
}

AddVariable.propTypes = {
  slip: PropTypes.object,
  allVarAdjType: PropTypes.object
};
