import React, { Component } from 'react';
import PropTypes from 'prop-types';
import NotificationAlert from 'react-notification-alert';
import 'react-notification-alert/dist/animate.css';

class Toast extends Component {
  componentDidMount() {
    this.refs.notify.notificationAlert(this.getOptions());
  }

  getOptions = () => ({
    place: 'br',
    message: <div className="notification-msg">{this.props.message}</div>,
    type: this.props.type,
    icon: '',
    autoDismiss: 5
  });

  render() {
    return (
      <div>
        <NotificationAlert ref="notify" />
      </div>
    );
  }
}

export default Toast;

Toast.propTypes = {
  message: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired
};
