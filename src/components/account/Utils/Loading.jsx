import React from 'react';
import Loader from 'react-loader-spinner';
import PropTypes from 'prop-types';

export default function Loading({ width, top }) {
  return (
    <Loader
      style={{ position: 'absolute', top: top || '40%', left: '50%' }}
      type="Oval"
      color="#124e2b"
      height={80}
      width={width || 70}
    />
  );
}

Loading.propTypes = {
  width: PropTypes.string,
  top: PropTypes.string
};
