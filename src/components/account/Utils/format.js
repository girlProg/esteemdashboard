const number = new Intl.NumberFormat('en-NG', { minimumFractionDigits: 2 });
export const formatNumber = n => number.format(n);

const currency = new Intl.NumberFormat('en-NG', {
  style: 'currency',
  currency: 'NGN'
});
export const formatCurrency = n => currency.format(n);
