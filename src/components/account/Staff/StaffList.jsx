import React from 'react';
import { Row, Col, Button } from 'reactstrap';
import PropTypes from 'prop-types';
import Datatable from 'react-bs-datatable';
import { css } from 'emotion';
import { useHistory, useRouteMatch } from 'react-router-dom';
import Loading from '../Utils/Loading';
import { formatNumber } from '../Utils/format';
// eslint-disable-next-line react/prop-types

export default function StaffList({ allStaff }) {
  const history = useHistory();
  const { url } = useRouteMatch();
  const header = [
    { title: 'S/N', prop: 'sn', sortable: true, filterable: true },
    {
      title: 'Action',
      prop: 'action',
      cell: row => (
        <Button className="btn-sm" color="primary" onClick={() => handleRowClick(row)}>
          View
        </Button>
      )
    },
    { title: 'Name', prop: 'name', sortable: true, filterable: true },
    {
      title: 'Salary Amount',
      prop: 'salaryAmount',
      sortable: true,
      filterable: true
    },
    { title: 'NHIS', prop: 'nhis', sortable: true, filterable: true },
    {
      title: 'Allowance for Heads',
      prop: 'allowanceforHeads',
      sortable: true,
      filterable: true
    }
  ];

  const handleRowClick = row => {
    const select = allStaff.results.find(staff => staff.id === row.id);
    history.push(`${url}/profile/${select.id}`);
  };

  function getTableBody() {
    const body = [];
    const sortedList = allStaff.results.sort((a, b) => {
      if (a.name < b.name) return -1;
      return 1;
    });

    let sn = 0;
    sortedList.forEach(staff => {
      body.push({
        sn: (sn += 1),
        id: staff.id,
        name: staff.name,
        salaryAmount: formatNumber(staff.salaryAmount),
        nhis: formatNumber(staff.nhis),
        allowanceforHeads: formatNumber(staff.allowanceforHeads)
      });
    });
    return body;
  }

  const classes = {
    theadCol: css`
      .table-datatable__root & {
        font-weight: bold;
      }
    `,
    tbodyRow: css`
      &:hover {
        background: #eaeaea;
      }
    `,
    table: css`
      display: block;
      overflow-x: auto;
      white-space: nowrap;
      margin-top: 2em;
      font-weight: 400;
    `
  };

  return (
    <Row>
      {allStaff ? (
        <Col xs={12} md={12}>
          <div className="page-title">
            <div className="float-left">
              <h1 className="title">Staff List</h1>
            </div>
          </div>
          <div className="col-12">
            <section className="box ">
              <header className="panel_header">
                <h2 className="title float-left">Staff List</h2>
              </header>
              <div className="content-body">
                <div className="row">
                  <div className="col-lg-12 dt-disp">
                    <Datatable tableHeaders={header} tableBody={getTableBody()} classes={classes} />
                  </div>
                </div>
              </div>
            </section>
          </div>
        </Col>
      ) : (
        <Loading />
      )}
    </Row>
  );
}

StaffList.propTypes = {
  allStaff: PropTypes.object
};
