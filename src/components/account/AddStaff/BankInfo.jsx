import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Label, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

export default function BankInfo({ isRequired, bankState, setBankState, banks }) {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const handleChange = e => {
    const { name, value } = e.target;
    setBankState({ ...bankState, [name]: value });
  };

  const toggle = () => setDropdownOpen(prevState => !prevState);

  const dropDownSelect = e =>
    setBankState(() => {
      const { innerText, value } = e.target;
      return {
        ...bankState,
        id: value,
        name: innerText
      };
    });

  return (
    <>
      <header className="panel_header" style={{ marginLeft: '-30px' }}>
        <h2 className="title">Bank Info</h2>
      </header>
      <div className="form-row">
        <div className="form-group col-md-12">
          <Label htmlFor="bank_name">Bank</Label>
          <Dropdown className="d-block ml-0" isOpen={dropdownOpen} toggle={toggle}>
            <DropdownToggle color={bankState.name ? 'primary' : 'secondary'} caret>
              {bankState.name || 'Select an option...'}
            </DropdownToggle>
            <DropdownMenu>
              <></>
              {banks &&
                banks.results.map(bank => (
                  <div key={bank.id}>
                    <DropdownItem value={bank.id} onClick={dropDownSelect}>
                      {bank.name}
                    </DropdownItem>
                    <DropdownItem divider></DropdownItem>
                  </div>
                ))}
            </DropdownMenu>
          </Dropdown>
        </div>
        <div className="form-group col-md-12">
          <label htmlFor="account_number">Account Number</label>
          <input
            type="number"
            // required={isRequired}
            className="form-control"
            id="account_number"
            name="number"
            // max length to 10 numbers
            onInput={e => (e.target.value = String(e.target.value).slice(0, 10))}
            value={bankState.number ? bankState.number : ''}
            onChange={handleChange}
          />
        </div>
      </div>
    </>
  );
}

BankInfo.propTypes = {
  isRequired: PropTypes.bool,
  bankState: PropTypes.object.isRequired,
  setBankState: PropTypes.func.isRequired,
  banks: PropTypes.object
};
