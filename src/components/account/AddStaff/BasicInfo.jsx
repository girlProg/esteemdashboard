/* eslint-disable react/jsx-boolean-value */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Label, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

export default function BasicInfo({ isRequired, basicState, setBasicState, allSchoolBranch, edit }) {
  const [dropdownOpen1, setDropdownOpen1] = useState(false);
  const [dropdownOpen2, setDropdownOpen2] = useState(false);
  const [dropdownOpen3, setDropdownOpen3] = useState(false);
  const [dropdownOpen4, setDropdownOpen4] = useState(false);

  const handleChange = e => {
    const { name, value } = e.target;
    setBasicState(prev => ({ ...prev, [name]: value.toUpperCase() }));
  };

  const handleDecimal = e => {
    const { name, value } = e.target;
    let num = parseFloat(value);
    if (value === '') {
      num = 0;
    }
    const numToTwodecimal = num.toFixed(2);
    setBasicState(prev => ({ ...prev, [name]: numToTwodecimal }));
  };

  const toggle1 = () => setDropdownOpen1(prevState => !prevState);
  const toggle2 = () => setDropdownOpen2(prevState => !prevState);
  const toggle3 = () => setDropdownOpen3(prevState => !prevState);
  const toggle4 = () => setDropdownOpen4(prevState => !prevState);

  const dropDownSelect = e => {
    const { name, value } = e.target;
    setBasicState(prev => ({
      ...prev,
      [name]: value
    }));
  };

  const branchDropDownSelect = e => {
    const { name, value, innerText } = e.target;
    setBasicState(prev => ({
      ...prev,
      [name]: value,
      branchName: innerText
    }));
  };

  return (
    <div className="form-row">
      <div className="form-group col-md-12">
        <Label htmlFor="name">Name</Label>
        <input
          type="text"
          required={isRequired}
          className="form-control"
          id="name"
          name="name"
          value={basicState.name ? basicState.name : ''}
          onChange={handleChange}
        />
      </div>
      {/* <div className="form-group col-md-12">
        <label htmlFor="pension_code">Pension Code</label>
        <input
          type="text"
          required={isRequired}
          className="form-control"
          id="pension_code"
          name="pension_code"
          value={basicState.pension_code}
          onChange={handleChange}
        />
      </div> */}
      <div className="form-group col-md-12">
        <Label htmlFor="salary_amount">Salary Amount</Label>
        <input
          type="number"
          required={isRequired}
          className="form-control"
          id="salary_amount"
          name="salaryAmount"
          onBlur={handleDecimal}
          value={basicState.salaryAmount ? basicState.salaryAmount : ''}
          onChange={handleChange}
        />
      </div>
      {/* <div className="form-group col-md-12">
        <Label htmlFor="gross_income">Gross Income</Label>
        <input
          type="number"
          required={isRequired}
          className="form-control"
          id="gross_income"
          name="grossIncome"
          onBlur={handleDecimal}
          value={basicState.grossIncome}
          onChange={handleChange}
        />
      </div> */}
      <div className="form-group col-md-12">
        <Label htmlFor="nhis">NHIS</Label>
        <input
          type="number"
          required={isRequired}
          className="form-control"
          id="nhis"
          name="nhis"
          onBlur={handleDecimal}
          value={basicState.nhis ? basicState.nhis : ''}
          onChange={handleChange}
        />
      </div>
      <div className="form-group col-md-12">
        <Label htmlFor="allowance_for_heads">Allowance for Heads</Label>
        <input
          type="number"
          required={isRequired}
          className="form-control"
          id="allowance_for_heads"
          name="allowanceforHeads"
          onBlur={handleDecimal}
          value={basicState.allowanceforHeads ? basicState.allowanceforHeads : ''}
          onChange={handleChange}
        />
      </div>
      {/* <div className="form-group col-md-12">
        <Label htmlFor="tax">Tax</Label>
        <input
          type="number"
          id="tax"
          name="tax"
          onBlur={handleDecimal}
          value={basicState.tax}
          onChange={handleChange}
          required={isRequired}
          className="form-control"
        />
      </div> */}
      <div className="form-group col-md-12">
        <Label htmlFor="is_current_staff">School Branch</Label>
        <Dropdown className="d-block ml-0" isOpen={dropdownOpen4} toggle={toggle4}>
          <DropdownToggle color={basicState.schoolBranch ? 'primary' : 'secondary'} caret>
            {basicState.branchName || 'Select an option...'}
          </DropdownToggle>
          <DropdownMenu>
            <></>
            {allSchoolBranch &&
              allSchoolBranch.results.map(branch => (
                <div key={branch.id}>
                  <DropdownItem name="schoolBranch" value={branch.id} onClick={branchDropDownSelect}>
                    {branch.name}
                  </DropdownItem>
                  <DropdownItem divider></DropdownItem>
                </div>
              ))}
          </DropdownMenu>
        </Dropdown>
      </div>
      {edit && (
        <>
          <div className="form-group col-md-12">
            <Label htmlFor="is_current_staff">Is Current Staff?</Label>
            <Dropdown className="d-block ml-0" isOpen={dropdownOpen1} toggle={toggle1}>
              <DropdownToggle color={basicState.isCurrentStaff ? 'primary' : 'secondary'} caret>
                {(basicState.isCurrentStaff === true || basicState.isCurrentStaff === 'true') && 'YES'}
                {(basicState.isCurrentStaff === false || basicState.isCurrentStaff === 'false') && 'NO'}
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem name="isCurrentStaff" value="true" onClick={dropDownSelect}>
                  YES
                </DropdownItem>
                <DropdownItem divider></DropdownItem>
                <DropdownItem name="isCurrentStaff" value="false" onClick={dropDownSelect}>
                  NO
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </div>
          <div className="form-group col-md-12">
            <Label htmlFor="is_accounts_staff">Is Accounts Staff?</Label>
            <Dropdown className="d-block ml-0" isOpen={dropdownOpen2} toggle={toggle2}>
              <DropdownToggle color={basicState.isAccountsStaff !== null ? 'primary' : 'secondary'} caret>
                {(basicState.isAccountsStaff === true || basicState.isAccountsStaff === 'true') && 'YES'}
                {(basicState.isAccountsStaff === false || basicState.isAccountsStaff === 'false') && 'NO'}
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem name="isAccountsStaff" value="true" onClick={dropDownSelect}>
                  YES
                </DropdownItem>
                <DropdownItem divider></DropdownItem>
                <DropdownItem name="isAccountsStaff" value="false" onClick={dropDownSelect}>
                  NO
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </div>
          <div className="form-group col-md-12">
            <Label htmlFor="is_pensioner">Is Pensioner?</Label>
            <Dropdown className="d-block ml-0" isOpen={dropdownOpen3} toggle={toggle3}>
              <DropdownToggle color={basicState.is_pensioner !== null ? 'primary' : 'secondary'} caret>
                {(basicState.is_pensioner === true || basicState.is_pensioner === 'true') && 'YES'}
                {(basicState.is_pensioner === false || basicState.is_pensioner === 'false') && 'NO'}
              </DropdownToggle>
              <DropdownMenu>
                <DropdownItem name="is_pensioner" value="true" onClick={dropDownSelect}>
                  YES
                </DropdownItem>
                <DropdownItem divider></DropdownItem>
                <DropdownItem name="is_pensioner" value="false" onClick={dropDownSelect}>
                  NO
                </DropdownItem>
              </DropdownMenu>
            </Dropdown>
          </div>
        </>
      )}
    </div>
  );
}

BasicInfo.propTypes = {
  isRequired: PropTypes.bool,
  basicState: PropTypes.object.isRequired,
  setBasicState: PropTypes.func.isRequired,
  allSchoolBranch: PropTypes.object,
  edit: PropTypes.bool
};
