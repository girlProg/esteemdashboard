import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Label, Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

export default function PfaInfo({ isRequired, pfaState, setPfaState, pfas }) {
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen(prevState => !prevState);

  const dropDownSelect = e =>
    setPfaState(() => {
      const { innerText, value } = e.target;
      return {
        id: value,
        name: innerText
      };
    });

  return (
    <>
      <header className="panel_header" style={{ marginLeft: '-30px' }}>
        <h2 className="title">PFA INFO</h2>
      </header>

      <div className="form-row mb-5">
        <div className="form-group col-md-12">
          <Label htmlFor="pension_collector">Pension Collector</Label>
          <Dropdown className="d-block ml-0" isOpen={dropdownOpen} toggle={toggle}>
            <DropdownToggle color={pfaState.name ? 'primary' : 'secondary'} caret>
              {pfas?.count === 0 ? 'No PFAs available' : pfaState.name || 'Select an option...'}
            </DropdownToggle>
            <DropdownMenu>
              {pfas?.count > 0 &&
                pfas.results.map(pfa => (
                  <div key={pfa.id}>
                    <DropdownItem value={pfa.id} onClick={dropDownSelect}>
                      {pfa.name}
                    </DropdownItem>
                    <DropdownItem divider></DropdownItem>
                  </div>
                ))}
            </DropdownMenu>
          </Dropdown>
        </div>
      </div>
    </>
  );
}

PfaInfo.propTypes = {
  isRequired: PropTypes.bool,
  pfaState: PropTypes.object.isRequired,
  setPfaState: PropTypes.func.isRequired,
  pfas: PropTypes.object
};
