import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button } from 'reactstrap';
import Loading from '../Utils/Loading';
import ButtonWithPopOver from '../ButtonWithPopOver';

export default function BankList({ banks, getSelectedBank, handleEdit, handleDelete, loading }) {
  return (
    <div>
      <div className="content">
        <Row>
          {loading ? (
            <Loading />
          ) : (
            <Col xs={12} md={12}>
              <div className="page-title">
                <div className="float-left">
                  <h1 className="title">Bank</h1>
                </div>
              </div>

              <div className="col-xl-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title float-left">All Banks</h2>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      <div className="col-12 d-flex flex-wrap">
                        {banks?.results.map(bank => (
                          <div className="col-md-6 col-lg-4 col-sm-12" key={bank.id}>
                            <div className="team-member w-100 h-80">
                              <div className="row margin-0">
                                <div className="team-info">
                                  <h3 className="name">{bank.name}</h3>
                                  <div>
                                    <Button color="warning" className="m-80" size="sm" onClick={handleEdit}>
                                      Edit
                                    </Button>
                                    <ButtonWithPopOver
                                      target={bank.id}
                                      getSelected={getSelectedBank}
                                      handleDelete={handleDelete}
                                    />
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        ))}
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </Col>
          )}
        </Row>
      </div>
    </div>
  );
}

BankList.propTypes = {
  banks: PropTypes.object,
  getSelectedBank: PropTypes.func.isRequired,
  handleEdit: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
};
