import React from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Button } from 'reactstrap';
import Loading from '../Utils/Loading';
import ButtonWithPopOver from '../ButtonWithPopOver';

export default function PFAList({ pfas, getSelectedPFA, handleEdit, handleDelete, loading }) {
  return (
    <div>
      <div className="content">
        <Row>
          {loading && <Loading />}
          {!loading && (
            <Col xs={12} md={12}>
              <div className="page-title">
                <div className="float-left">
                  <h1 className="title">Pension Fund Administrators</h1>
                </div>
              </div>

              <div className="col-xl-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title float-left">All Pfa</h2>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      {pfas?.count > 0 ? (
                        <div className="col-12 d-flex flex-wrap">
                          {pfas?.results.map(pfa => (
                            <div className="col-md-6 col-lg-6 col-sm-12" key={pfa.id}>
                              <div className="team-member w-100 h-80 pfa-card">
                                <div className="row margin-0">
                                  <div className="team-info">
                                    <h3 className="name">{pfa.name}</h3>
                                    <p className="pfa-details">Address: {pfa.address || 'N/A'}</p>
                                    <p className="pfa-details">Pension ID: {pfa.pensionID || 'N/A'}</p>
                                    <div>
                                      <Button color="warning" size="sm" onClick={e => handleEdit(e)}>
                                        EDIT
                                      </Button>
                                      <ButtonWithPopOver
                                        target={pfa.id}
                                        getSelected={getSelectedPFA}
                                        handleDelete={handleDelete}
                                      />
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          ))}
                        </div>
                      ) : (
                        <div className="col-12 d-flex flex-wrap justify-content-center">
                          <h5 className="text-center">No Pension Fund Administrators</h5>
                        </div>
                      )}
                    </div>
                  </div>
                </section>
              </div>
            </Col>
          )}
          {/* {!loading && pfas?.count === 0 && (
            <Col>
              <div>
                <h3 className="text-center mt-4">NO pension fund administrators</h3>
              </div>
            </Col>
          )} */}
        </Row>
      </div>
    </div>
  );
}

PFAList.propTypes = {
  pfas: PropTypes.object,
  getSelectedPFA: PropTypes.func.isRequired,
  handleEdit: PropTypes.func.isRequired,
  handleDelete: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired
};
