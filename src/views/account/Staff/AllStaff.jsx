import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actions } from '../../../store/actions/staffActions';
import StaffList from '../../../components/account/Staff/StaffList';
import Toast from '../../../components/account/Utils/Toast';

export default function AllStaff() {
  const allStaff = useSelector(staff => staff.staffReducer).staff;
  const { status } = useSelector(state => state.staffReducer);

  const dispatch = useDispatch();
  const [showToast, setShowToast] = useState(false);
  const [toast, setToast] = useState({ type: '', message: '' });

  useEffect(() => {
    actions.getAllStaff(dispatch);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (status === 201) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Staff added successfully.' });
    }

    if (status === 204) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Staff deleted successfully.' });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  return (
    <div>
      <div className="content">
        <StaffList allStaff={allStaff} />
        {showToast && <Toast type={toast.type} message={toast.message} />}
      </div>
    </div>
  );
}
