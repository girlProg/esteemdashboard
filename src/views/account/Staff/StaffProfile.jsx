import React, { useState, useEffect } from 'react';
import {
  TabContent,
  Table,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  CardText,
  Card,
  CardTitle,
  Row,
  Col,
  Button
} from 'reactstrap';
import classnames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import { formatNumber } from '../../../components/account/Utils/format';
import Toast from '../../../components/account/Utils/Toast';
import ButtonWithPopOver from '../../../components/account/ButtonWithPopOver';
import Loading from '../../../components/account/Utils/Loading';

// eslint-disable-next-line react/prop-types
function StaffProfile() {
  const { staff: allStaff, status } = useSelector(staff => staff.staffReducer);
  const dispatch = useDispatch();
  const { id } = useParams();
  const history = useHistory();
  const profile = allStaff?.results?.find(staff => staff.id === Number(id));
  const [showToast, setShowToast] = useState(false);
  const [toast, setToast] = useState({ type: '', message: '' });

  const [activeTab, setActiveTab] = useState('1');
  const toggle = tab => {
    if (activeTab !== tab) {
      setActiveTab(tab);
    }
  };

  useEffect(() => {
    async function fetchData() {
      await actions.getAllStaff(dispatch);
    }
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (status === 200) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Staff edited successfully.' });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  const handleDelete = async () => {
    await actions.deleteStaff(dispatch, profile);
    history.goBack();
  };

  return (
    <div>
      <div className="content mt-5">
        <Row>
          {profile ? (
            <Col xs={12} md={8}>
              <div className="col-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title float-left">Staff Profile</h2>
                    <Button color="primary" className="float-right mt-3" onClick={() => history.goBack()}>
                      <i className="i-arrow-left-circle"></i> &nbsp; <span>Back to list</span>
                    </Button>
                  </header>

                  <div className="content-body">
                    <div className="row">
                      <div className="col-lg-12 mt-4">
                        <h4>{profile.name}</h4>
                        <div>
                          <Nav tabs>
                            {/* Basic Info Tab */}
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: activeTab === '1'
                                })}
                                onClick={() => {
                                  toggle('1');
                                }}
                              >
                                <i className="i-user"></i> Basic Info
                              </NavLink>
                            </NavItem>
                            {/* Bank Info Tab */}
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: activeTab === '2'
                                })}
                                onClick={() => {
                                  toggle('2');
                                }}
                              >
                                <i className="i-credit-card"></i> Bank Info
                              </NavLink>
                            </NavItem>
                            {/* PFA Info Tab */}
                            <NavItem>
                              <NavLink
                                className={classnames({
                                  active: activeTab === '3'
                                })}
                                onClick={() => {
                                  toggle('3');
                                }}
                              >
                                <i className="i-briefcase"></i> PFA Info
                              </NavLink>
                            </NavItem>
                          </Nav>
                          <TabContent activeTab={activeTab}>
                            {/* Basic Info Content */}
                            <TabPane tabId="1">
                              <Row>
                                <Col sm="12">
                                  <Table striped>
                                    <tbody>
                                      <tr>
                                        <td className="bold">NAME</td>
                                        <td>{profile.name}</td>
                                      </tr>
                                      <tr>
                                        <td className="bold">SCHOOL BRANCH</td>
                                        <td>{profile.schoolBranch ? profile.schoolBranch.name : 'N/A'}</td>
                                      </tr>
                                      <tr>
                                        <td className="bold">SALARY AMOUNT</td>
                                        <td>
                                          &#8358;
                                          {formatNumber(profile.salaryAmount)}
                                        </td>
                                      </tr>
                                      <tr>
                                        <td className="bold">GROSS INCOME</td>
                                        <td>
                                          &#8358;
                                          {formatNumber(profile.grossIncome)}
                                        </td>
                                      </tr>
                                      <tr>
                                        <td className="bold">NHIS</td>
                                        <td>&#8358;{formatNumber(profile.nhis)}</td>
                                      </tr>
                                      <tr>
                                        <td className="bold">ALLOWANCE FOR HEADS</td>
                                        <td>
                                          &#8358;
                                          {formatNumber(profile.allowanceforHeads)}
                                        </td>
                                      </tr>
                                      <tr>
                                        <td className="bold">TAX</td>
                                        <td>&#8358;{formatNumber(profile.tax)}</td>
                                      </tr>
                                      <tr>
                                        <td className="bold">PENSION CODE</td>
                                        <td>{profile.pension_code ? profile.pension_code : 'N/A'}</td>
                                      </tr>
                                      <tr>
                                        <td className="bold">CURRENT STAFF</td>
                                        <td>{profile.isCurrentStaff ? 'YES' : 'NO'}</td>
                                      </tr>
                                      <tr>
                                        <td className="bold">PENSIONER</td>
                                        <td>{profile.is_pensioner ? 'YES' : 'NO'}</td>
                                      </tr>
                                      <tr>
                                        <td className="bold">ACCOUNTS STAFF</td>
                                        <td>{profile.isAccountsStaff ? 'YES' : 'NO'}</td>
                                      </tr>
                                    </tbody>
                                  </Table>
                                </Col>
                              </Row>
                            </TabPane>
                            {/* Bank Info Content */}
                            <TabPane tabId="2">
                              <Row>
                                <Col sm="12">
                                  {profile.bankAccount[0] ? (
                                    <Card body>
                                      <CardTitle>{profile.bankAccount[0].bank.name}</CardTitle>
                                      <CardText>ACCOOUNT NUMBER: {profile.bankAccount[0].number}</CardText>
                                    </Card>
                                  ) : (
                                    'Information not available.'
                                  )}
                                </Col>
                              </Row>
                            </TabPane>
                            {/* PFA Info Content */}
                            <TabPane tabId="3">
                              <Row>
                                <Col sm="12">
                                  {profile.pension_collector ? (
                                    <Card body>
                                      <CardTitle>{profile.pension_collector.name}</CardTitle>
                                      <CardText>ADDRESS: {profile.pension_collector.address}</CardText>
                                      <CardText>PENSION ID: {profile.pension_collector.pensionID}</CardText>
                                    </Card>
                                  ) : (
                                    'Information not available.'
                                  )}
                                </Col>
                              </Row>
                            </TabPane>
                          </TabContent>
                        </div>
                      </div>
                    </div>
                    <div className="mt-4">
                      <Button size="lg" color="warning" onClick={() => history.push(`edit/${profile.id}`)}>
                        EDIT
                      </Button>
                      <ButtonWithPopOver size="lg" target="popOver" handleDelete={handleDelete} />
                    </div>
                  </div>
                </section>
              </div>
            </Col>
          ) : (
            <Loading />
          )}
          {showToast && <Toast type={toast.type} message={toast.message} />}
        </Row>
      </div>
    </div>
  );
}

export default StaffProfile;
