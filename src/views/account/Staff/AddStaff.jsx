import React, { useState, useEffect } from 'react';
import { Row, Col } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import BasicInfo from '../../../components/account/AddStaff/BasicInfo';
import BankInfo from '../../../components/account/AddStaff/BankInfo';
import PfaInfo from '../../../components/account/AddStaff/PfaInfo';

function AddStaff() {
  const history = useHistory();
  const dispatch = useDispatch();
  const { allSchoolBranch, pfas, banks, newstaff, loading, status } = useSelector(state => state.staffReducer);

  const [isRequired] = useState(true);
  const [basicState, setBasicState] = useState({
    name: '',
    pension_code: '',
    grossIncome: 0,
    salaryAmount: '',
    nhis: '',
    allowanceforHeads: '',
    tax: 0,
    schoolBranch: '',
    isCurrentStaff: true,
    isAccountsStaff: false,
    is_pensioner: true
  });
  const [bankState, setBankState] = useState({
    id: '',
    name: '',
    number: ''
  });
  const [pfaState, setPfaState] = useState({
    id: '',
    name: ''
  });

  useEffect(() => {
    async function fetchData() {
      await actions.getAllPFAs(dispatch);
      await actions.getAllBanks(dispatch);
      await actions.getAllSchoolBranch(dispatch);
    }
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    const bankObj = {
      bank: { id: bankState.id },
      number: bankState.number,
      staff: newstaff && newstaff.id
    };

    if (bankState.id && bankState.number) {
      actions.addBankAccount(dispatch, bankObj);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [newstaff, status]);

  async function handleSubmit(e) {
    e.preventDefault();
    delete basicState.branchName;

    const staffObj = {
      user: null,
      ...basicState,
      ...(basicState.schoolBranch ? { schoolBranch: { id: basicState.schoolBranch } } : { schoolBranch: undefined }),
      ...(pfaState.id ? { pension_collector: { id: pfaState.id } } : { pension_collector: undefined })
    };

    await actions.addStaff(dispatch, staffObj);
    history.push('staff');
  }

  return (
    <div>
      <div className="content">
        <Row>
          <Col xs={12} md={12}>
            <div className="page-title">
              <div className="float-left">
                <h1 className="title">Add Staff</h1>
              </div>
            </div>
            <div className="col-12">
              <section className="box ">
                <header className="panel_header">
                  <h2 className="title float-left">Basic Info</h2>
                </header>
                <div className="content-body">
                  <div className="row">
                    <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
                      <form onSubmit={handleSubmit}>
                        <BasicInfo
                          isRequired={isRequired}
                          basicState={basicState}
                          setBasicState={setBasicState}
                          handleSubmit={handleSubmit}
                          allSchoolBranch={allSchoolBranch}
                        />
                        <BankInfo
                          isRequired={!isRequired}
                          banks={banks}
                          bankState={bankState}
                          setBankState={setBankState}
                          handleSubmit={handleSubmit}
                        />
                        <PfaInfo
                          isRequired={!isRequired}
                          pfaState={pfaState}
                          setPfaState={setPfaState}
                          handleSubmit={handleSubmit}
                          pfas={pfas}
                        />
                        <button type="submit" className="btn btn-primary">
                          ADD STAFF
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default AddStaff;
