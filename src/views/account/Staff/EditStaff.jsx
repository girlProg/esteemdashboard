import React, { useState, useEffect } from 'react';
import { Row, Col, Button } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import BasicInfo from '../../../components/account/AddStaff/BasicInfo';
import BankInfo from '../../../components/account/AddStaff/BankInfo';
import PfaInfo from '../../../components/account/AddStaff/PfaInfo';
import Loading from '../../../components/account/Utils/Loading';

function EditStaff() {
  const { id } = useParams();
  const history = useHistory();
  const dispatch = useDispatch();
  const [isRequired] = useState(false);

  useEffect(() => {
    actions.getAllStaff(dispatch);
    actions.getAllPFAs(dispatch);
    actions.getAllBanks(dispatch);
    actions.getAllSchoolBranch(dispatch);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const { staff: allStaffs, pfas, banks, allSchoolBranch, loading } = useSelector(pfa => pfa.staffReducer);
  const profile = allStaffs?.results?.find(staff => staff.id === Number(id));
  const [basicState, setBasicState] = useState({});
  const [bankState, setBankState] = useState({});
  const [pfaState, setPfaState] = useState({});

  useEffect(() => {
    if (profile) {
      setBasicState({
        name: profile.name,
        // pension_code: profile.pension_code,
        salaryAmount: profile.salaryAmount,
        nhis: profile.nhis,
        allowanceforHeads: profile.allowanceforHeads,
        schoolBranch: profile.schoolBranch?.id,
        branchName: profile.schoolBranch?.name,
        isCurrentStaff: profile.isCurrentStaff,
        isAccountsStaff: profile.isAccountsStaff,
        is_pensioner: profile.is_pensioner
      });
      setBankState({
        number: profile.bankAccount[0]?.number,
        id: profile.bankAccount[0]?.bank.id,
        name: profile.bankAccount[0]?.bank.name
      });
      setPfaState({
        id: profile.pension_collector?.id,
        name: profile.pension_collector?.name
      });
    }
  }, [profile]);

  async function handleSubmit(e) {
    e.preventDefault();
    delete basicState.branchName;

    const staffObj = {
      ...basicState,
      id: profile.id,
      ...(basicState.schoolBranch ? { schoolBranch: { id: basicState.schoolBranch } } : { schoolBranch: undefined }),
      ...(pfaState.id ? { pension_collector: { id: pfaState.id } } : { pension_collector: undefined }),
      ...(bankState.id
        ? { bankAccount: [{ bank: { id: bankState.id }, number: bankState.number, staff: profile.id }] }
        : { bankAccount: undefined })
    };

    await actions.editStaff(dispatch, staffObj);
    history.goBack();
  }

  return (
    <div>
      <div className="content">
        <Row>
          {loading ? (
            <Loading />
          ) : (
            <Col xs={12} md={12}>
              <div className="page-title">
                <div className="float-left">
                  <h1 className="title">EDIT Staff</h1>
                </div>
                <Button color="primary" className="float-right mt-3" onClick={() => history.goBack()}>
                  <i className="i-arrow-left-circle"></i> &nbsp; <span>Back to profile</span>
                </Button>
              </div>
              <div className="col-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title float-left">Basic Info</h2>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
                        <form onSubmit={handleSubmit}>
                          <BasicInfo
                            isRequired={isRequired}
                            basicState={basicState}
                            setBasicState={setBasicState}
                            handleSubmit={handleSubmit}
                            allSchoolBranch={allSchoolBranch}
                            edit
                          />
                          <BankInfo
                            isRequired={isRequired}
                            banks={banks}
                            bankState={bankState}
                            setBankState={setBankState}
                            handleSubmit={handleSubmit}
                          />
                          <PfaInfo
                            isRequired={isRequired}
                            pfaState={pfaState}
                            setPfaState={setPfaState}
                            handleSubmit={handleSubmit}
                            pfas={pfas}
                          />
                          <button type="submit" className="btn btn-primary">
                            UPDATE
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </Col>
          )}
        </Row>
      </div>
    </div>
  );
}

export default EditStaff;
