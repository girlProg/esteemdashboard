import React, { useState, useRef, useEffect } from 'react';
import { Row, Col } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { actions } from '../../../store/actions/authentication';
import Toast from '../../../components/account/Utils/Toast';
import Loading from '../../../components/account/Utils/Loading';

function Login() {
  const dispatch = useDispatch();
  const { loading, error } = useSelector(state => state.authentication);
  const usernameRef = useRef(null);
  const passwordRef = useRef(null);

  const [state, setState] = useState({
    username: '',
    password: ''
  });

  useEffect(() => {
    if (error) usernameRef.current.focus();
  }, [error]);

  const handleChange = e => {
    const { name, value } = e.target;
    setState({ ...state, [name]: value });
  };

  const style = {
    border: error && '1px solid red'
  };

  const handleSubmit = async e => {
    e.preventDefault();
    const { username, password } = state;
    actions.login(dispatch, username, password);
    setState({
      username: '',
      password: ''
    });
  };

  return (
    <div>
      <div className="content">
        <Row>
          <Col xs={12} md={12}>
            <div className="page-title">
              <div className="text-center">
                <h1 className="title">ELC ACCOUNTS</h1>
              </div>
            </div>

            <div className="d-flex justify-content-center">
              <section /** className="box" */>
                <div className="content-body">
                  <div className="row align-items-center justify-content-center">
                    <div className="col-8">
                      <form>
                        {loading ? (
                          <Loading />
                        ) : (
                          <>
                            <div className="form-row mt-5">
                              <div className="form-group col-md-12">
                                <label htmlFor="username">Username</label>
                                <input
                                  type="text"
                                  required
                                  className="form-control"
                                  id="username"
                                  name="username"
                                  value={state.username}
                                  onChange={handleChange}
                                  ref={usernameRef}
                                  style={style}
                                  onBlur={() => (usernameRef.current.style.border = 'none')}
                                />
                              </div>
                              <div className="form-group col-md-12">
                                <label htmlFor="password">Password</label>
                                <input
                                  type="password"
                                  className="form-control"
                                  id="password"
                                  name="password"
                                  value={state.password}
                                  onChange={handleChange}
                                  ref={passwordRef}
                                  style={style}
                                  onBlur={() => (passwordRef.current.style.border = 'none')}
                                />
                              </div>
                            </div>
                            <button onClick={handleSubmit} type="submit" className="btn btn-primary btn-block">
                              LOG IN
                            </button>
                            {/* {loading === false && error === false && <Redirect to="/esteem/home"></Redirect>} */}
                            {/* {console.log(localStorage.getItem('token'))} */}
                            {localStorage.getItem('token') && <Redirect to="/esteem/home"></Redirect>}
                            {loading === false && error === true && (
                              <Toast type="danger" place="bc" message="Invalid username or password." />
                            )}
                            <p className="d-flex justify-content-between mt-2">
                              <Link to="#">Forgot password?</Link>
                              <Link to="">Sign Up</Link>
                            </p>
                          </>
                        )}
                      </form>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default Login;
