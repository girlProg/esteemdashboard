import React from 'react';
import { Redirect } from 'react-router-dom';

export const PrivateRoute = props => {
  // localStorage.removeItem('token')
  const isloggedin = localStorage.getItem('token');
  const view = isloggedin ? <Redirect to="/esteem/staff" /> : <Redirect to="/esteem/login" />;
  return view;
};
