import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import Toast from '../../../components/account/Utils/Toast';
import VariableTypeList from '../../../components/account/VariableAdjustment/VariableTypeList';

function AllVariableAdjustment() {
  const history = useHistory();
  const { url } = useRouteMatch();
  const dispatch = useDispatch();
  const { allVarAdjType, status, loading } = useSelector(state => state.staffReducer);
  const [selectedAdj, setSelectedAdj] = useState(null);
  const [showToast, setShowToast] = useState(false);
  const [toast, setToast] = useState({ type: '', message: '' });

  useEffect(() => {
    async function fetchData() {
      await actions.getVarAdjType(dispatch);
    }
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (status === 200) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Adjustment edited successfully.' });
    }

    if (status === 201) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Adjustment added successfully.' });
    }

    if (status === 204) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Adjustment deleted successfully.' });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  const handleEdit = e => {
    const name = e.target.parentElement.parentElement.firstChild.textContent;
    const select = allVarAdjType?.results.find(adj => adj.name === name);
    history.push(`${url}/edit/${select.id}`);
  };

  const getSelectedAdj = e => {
    const name = e.target.parentElement.parentElement.firstChild.textContent;
    const adj = allVarAdjType.results.find(adj => name === adj.name);
    setSelectedAdj(adj);
  };

  const handleDelete = async () => {
    await actions.deleteVarAdjType(dispatch, selectedAdj);
    history.go(0);
  };

  return (
    <div className="content">
      <VariableTypeList
        loading={loading}
        getSelectedAdj={getSelectedAdj}
        allVarAdjType={allVarAdjType}
        handleDelete={handleDelete}
        handleEdit={handleEdit}
      />
      {showToast && <Toast type={toast.type} message={toast.message} />}
    </div>
  );
}

export default AllVariableAdjustment;
