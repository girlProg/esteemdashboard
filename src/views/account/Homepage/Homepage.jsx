import React from 'react';
import logo from '../../../assets/img/logo-color.png';

const Homepage = () => (
  <div className="d-flex align-items-center justify-content-center" style={{ marginTop: '12%', marginBottom: '5%' }}>
    <img src={logo} alt="elc logo" />
  </div>
);

export default Homepage;
