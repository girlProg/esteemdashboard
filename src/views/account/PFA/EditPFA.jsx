import React, { useState, useEffect } from 'react';
import { Row, Col, Button } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import Loading from '../../../components/account/Utils/Loading';

export default function EditPFA() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();
  const { pfas, loading } = useSelector(state => state.staffReducer);
  const selectedPfa = pfas?.results.find(pfa => pfa.id === Number(id));

  const [pfaState, setPfaState] = useState({
    id: '',
    name: '',
    address: '',
    pensionID: ''
  });

  useEffect(() => {
    if (selectedPfa) {
      setPfaState({
        id: selectedPfa.id,
        name: selectedPfa.name,
        address: selectedPfa.address,
        pensionID: selectedPfa.pensionID
      });
    }
  }, [selectedPfa]);

  useEffect(() => {
    async function fetchData() {
      await actions.getAllPFAs(dispatch);
    }
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChange = e => {
    const { name, value } = e.target;
    setPfaState({ ...pfaState, [name]: value });
  };

  const handleEditSave = async e => {
    e.preventDefault();
    await actions.editPfa(dispatch, pfaState);
    history.goBack();
  };

  return (
    <div>
      <div className="content">
        <Row>
          {loading ? (
            <Loading />
          ) : (
            <Col xs={12} md={12}>
              <div className="page-title">
                <div className="float-left">
                  <h1 className="title">Edit Pfa</h1>
                </div>
              </div>

              <div className="col-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title float-left">Pension Fund Administrator</h2>
                    <Button
                      color="primary"
                      className="float-right mt-3"
                      onClick={() => history.push('/esteem/pfa')}
                      size="sm"
                    >
                      <i className="i-arrow-left-circle"></i> &nbsp; <span>Back to list</span>
                    </Button>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
                        <form onSubmit={handleEditSave}>
                          <div className="form-row">
                            <div className="form-group col-md-12">
                              <label htmlFor="pfa_name">Name</label>
                              <input
                                type="text"
                                required={false}
                                className="form-control"
                                id="pfa_name"
                                name="name"
                                value={pfaState.name}
                                onChange={handleChange}
                                placeholder=""
                              />
                            </div>
                            <div className="form-group col-md-12">
                              <label htmlFor="pfa_address">Address</label>
                              <input
                                type="text"
                                required={false}
                                className="form-control"
                                id="pfa_address"
                                name="address"
                                value={pfaState.address}
                                onChange={handleChange}
                              />
                            </div>
                            <div className="form-group col-md-12">
                              <label htmlFor="pension_id">Pension ID</label>
                              <input
                                type="text"
                                required={false}
                                className="form-control"
                                id="pension_id"
                                name="pensionID"
                                value={pfaState.pensionID}
                                onChange={handleChange}
                              />
                            </div>
                          </div>
                          <button type="submit" className="btn btn-primary">
                            UPDATE
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </Col>
          )}
        </Row>
      </div>
    </div>
  );
}
