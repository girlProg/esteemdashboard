import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import Toast from '../../../components/account/Utils/Toast';
import PFAList from '../../../components/account/PFA/PFAList';

export default function AllPFA() {
  const history = useHistory();
  const { url } = useRouteMatch();
  const dispatch = useDispatch();
  const { pfas, status, loading } = useSelector(state => state.staffReducer);
  const [selectedPFA, setSelectedPFA] = useState(null);
  const [showToast, setShowToast] = useState(false);
  const [toast, setToast] = useState({ type: '', message: '' });

  useEffect(() => {
    async function fetchData() {
      await actions.getAllPFAs(dispatch);
    }
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (status === 200) {
      setShowToast(true);
      setToast({ type: 'success', message: 'PFA edited successfully.' });
    }

    if (status === 201) {
      setShowToast(true);
      setToast({ type: 'success', message: 'PFA added successfully.' });
    }

    if (status === 204) {
      setShowToast(true);
      setToast({ type: 'success', message: 'PFA deleted successfully.' });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  const handleEdit = e => {
    const name = e.target.parentElement.parentElement.firstChild.textContent;
    const select = pfas.results.find(pfa => pfa.name === name);
    history.push(`${url}/edit/${select.id}`);
  };

  const getSelectedPFA = e => {
    const name = e.target.parentElement.parentElement.firstChild.textContent;
    const pfa = pfas.results.find(pfa => name === pfa.name);
    setSelectedPFA(pfa);
  };

  const handleDelete = async () => {
    await actions.deletePfa(dispatch, selectedPFA);
    history.go(0);
  };

  return (
    <div className="content">
      <PFAList
        loading={loading}
        pfas={pfas}
        getSelectedPFA={getSelectedPFA}
        handleDelete={handleDelete}
        handleEdit={handleEdit}
      />
      {showToast && <Toast type={toast.type} message={toast.message} />}
    </div>
  );
}
