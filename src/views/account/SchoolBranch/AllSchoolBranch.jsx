import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useRouteMatch, useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import Toast from '../../../components/account/Utils/Toast';
import BranchList from '../../../components/account/SchoolBranch/BranchList';

export default function AllSchoolBranch() {
  const history = useHistory();
  const { url } = useRouteMatch();
  const dispatch = useDispatch();
  const { allSchoolBranch, status, loading } = useSelector(state => state.staffReducer);
  const [selectedBranch, setSelectedBranch] = useState(null);
  const [showToast, setShowToast] = useState(false);
  const [toast, setToast] = useState({ type: '', message: '' });

  useEffect(() => {
    async function fetchData() {
      await actions.getAllSchoolBranch(dispatch);
    }
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (status === 200) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Branch edited successfully.' });
    }

    if (status === 201) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Branch added successfully.' });
    }

    if (status === 204) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Branch deleted successfully.' });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  const handleEdit = e => {
    const name = e.target.parentElement.parentElement.firstChild.textContent;
    const select = allSchoolBranch?.results.find(branch => branch.name === name);
    history.push(`${url}/edit/${select.id}`);
  };

  const getSelectedBranch = e => {
    const name = e.target.parentElement.parentElement.firstChild.textContent;
    const branch = allSchoolBranch.results.find(branch => name === branch.name);
    setSelectedBranch(branch);
  };

  const handleDelete = async () => {
    await actions.deleteSchoolBranch(dispatch, selectedBranch);
    history.go(0);
  };

  return (
    <div className="content">
      <BranchList
        loading={loading}
        getSelectedBranch={getSelectedBranch}
        allSchoolBranch={allSchoolBranch}
        handleDelete={handleDelete}
        handleEdit={handleEdit}
      />
      {showToast && <Toast type={toast.type} message={toast.message} />}
    </div>
  );
}
