import React, { useState } from 'react';
import { Row, Col } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import Loading from '../../../components/account/Utils/Loading';

function AddBranch() {
  const history = useHistory();
  const dispatch = useDispatch();
  const { loading } = useSelector(state => state.staffReducer);

  const [state, setState] = useState({
    name: '',
    pensionID: ''
  });

  const handleChange = e => {
    const { name, value } = e.target;
    setState({ ...state, [name]: value });
  };

  const handleSubmit = async e => {
    e.preventDefault();
    await actions.addSchoolBranch(dispatch, state);
    history.push('branch');
  };

  return (
    <div>
      <div className="content">
        <Row>
          {loading ? (
            <Loading />
          ) : (
            <Col xs={12} md={12}>
              <div className="page-title">
                <div className="float-left">
                  <h1 className="title">Add School Branch</h1>
                </div>
              </div>

              <div className="col-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title float-left">School Branch</h2>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
                        <form onSubmit={handleSubmit}>
                          <div className="form-row">
                            <div className="form-group col-md-12">
                              <label htmlFor="name">Name</label>
                              <input
                                type="text"
                                required
                                className="form-control"
                                id="name"
                                name="name"
                                value={state.name}
                                onChange={handleChange}
                              />
                            </div>
                            <div className="form-group col-md-12">
                              <label htmlFor="pension_id">Pension ID</label>
                              <input
                                type="text"
                                className="form-control"
                                id="pension_id"
                                name="pensionID"
                                value={state.pensionID}
                                onChange={handleChange}
                              />
                            </div>
                          </div>
                          <button type="submit" className="btn btn-primary">
                            SAVE
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </Col>
          )}
        </Row>
      </div>
    </div>
  );
}

export default AddBranch;
