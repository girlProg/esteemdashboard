import React, { useEffect } from 'react';
import ReactTable from 'react-table-v6';
import 'react-table-v6/react-table.css';
import { Row, Col, Button } from 'reactstrap';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useParams, useRouteMatch } from 'react-router-dom';
import { formatNumber } from '../../../components/account/Utils/format';
import Loading from '../../../components/account/Utils/Loading';
import GeneratePDF from '../../../components/account/Voucher/GeneratePDF';
import { actions } from '../../../store/actions/staffActions';

function Payslip() {
  const { id } = useParams();
  const history = useHistory();
  const { url } = useRouteMatch();
  const dispatch = useDispatch();
  const { payslip, varAdj, loading } = useSelector(state => state.staffReducer);
  const slip = payslip?.results?.find(eachslip => eachslip.id === Number(id));

  const monthYear = new Date(localStorage.getItem('monthYear'));
  const month = monthYear.getMonth() + 1;
  const year = monthYear.getFullYear();

  useEffect(() => {
    async function fetchData() {
      await actions.getPayslip(dispatch, month, year);
      await actions.getVarAdj(dispatch, month, year);
    }
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const columns = [
    {
      Header: 'Description',
      accessor: 'description',
      style: { backgroundColor: '#eee', textAlign: 'center' }
    },
    { Header: 'Amount (N)', style: { textAlign: 'right' }, accessor: 'amount' }
  ];

  const body = [
    {
      description: 'Basic',
      amount: formatNumber(slip?.basic[0].amount)
      // constant: formatNumber(slip?.basic[0].constant * 100)
    },
    {
      description: 'Housing',
      amount: formatNumber(slip?.housing[0]?.amount)
      // constant: formatNumber(slip.housing[0].constant * 100 )
    },
    {
      description: 'Transport',
      amount: formatNumber(slip?.transport[0]?.amount)
      // constant: formatNumber(slip.transport[0].constant * 100)
    },
    {
      description: 'Meal',
      amount: formatNumber(slip?.meal[0]?.amount)
      // constant: formatNumber(slip.meal[0].constant * 100)
    },
    {
      description: 'Utility',
      amount: formatNumber(slip?.utility[0]?.amount)
      // constant: formatNumber(slip.utility[0].constant * 100)
    },
    {
      description: 'Education',
      amount: formatNumber(slip?.education[0]?.amount)
      // constant: formatNumber(slip.education[0].constant * 100)
    },
    {
      description: 'Dressing',
      amount: formatNumber(slip?.dressing[0]?.amount)
      // constant: formatNumber(slip.dressing[0].constant * 100)
    },
    {
      description: 'Medical',
      amount: formatNumber(slip?.medical[0]?.amount)
      // constant: formatNumber(slip.medical[0].constant * 100)
    },
    {
      description: 'Salary Amount',
      amount: formatNumber(slip?.salaryAmount)
    },
    {
      description: 'Allowance for Heads',
      amount: formatNumber(slip?.allowanceforHeads)
    },
    {
      description: 'Gross Income',
      amount: formatNumber(slip?.grossIncome)
    },
    {
      description: 'Pension',
      amount: formatNumber(slip?.pension[0]?.amount)
      // constant: formatNumber(slip.pension[0].constant * 100)
    },
    { description: 'NHIS', amount: formatNumber(slip?.nhis) },
    { description: 'Tax', amount: formatNumber(slip?.tax) },
    {
      description: 'Credit to Bank',
      amount: formatNumber(slip?.credittobank)
    }
  ];

  const index = body.length - 1;
  if (varAdj?.results) {
    varAdj.results.forEach(adj => {
      if (adj.payslip === slip.id) {
        const { name } = adj.name;
        // place incoming variables before Credit to Bank
        body.splice(index, 0, {
          description: name,
          amount: formatNumber(adj.amount)
        });
      }
    });
  }

  // calculate sum of all amount
  // const amount = body.map(data => Number(data.amount.replace(',', '')));
  // const total = amount.reduce((sum, num) => sum + num).toFixed(2);
  // body[body.length - 1].amount = formatNumber(total);

  // make total row bold
  const getTrProps = (state, rowInfo, instance) => {
    if (rowInfo) {
      return {
        style: {
          fontWeight: rowInfo.row.description === 'Credit to Bank' && 'bold',
          fontStyle: rowInfo.row._viewIndex > 13 && rowInfo.row.description !== 'Credit to Bank' && 'italic'
        }
      };
    }
    return {};
  };

  return (
    <div>
      <div className="content mt-5">
        <Row>
          {loading && <Loading />}
          <Col xs={12} md={8}>
            {!loading && !slip && (
              <div className="text-center d-flex justify-content-center">
                <div>
                  <h3>No payslip for this staff</h3>
                  <p className="link" onClick={() => history.goBack()}>
                    Go back
                  </p>
                </div>
              </div>
            )}
            {slip && (
              <div className="col-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title">
                      {`PAYSLIP FOR THE MONTH OF ${monthYear.toLocaleString('default', {
                        month: 'long'
                      })}, ${monthYear.getFullYear()}`}
                    </h2>
                    <Button color="primary" className="float-right mt-3" onClick={() => history.goBack()}>
                      <i className="i-arrow-left-circle"></i> &nbsp; <span>Back</span>
                    </Button>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      <div className="col-lg-12 mt-4">
                        <h4>{slip?.staff.name.toUpperCase()}</h4>
                        <ReactTable
                          minRows={0}
                          showPagination={false}
                          data={body}
                          noDataText="No Data"
                          columns={columns}
                          getTrProps={getTrProps}
                        />
                      </div>
                    </div>
                    <div className="mt-4">
                      <Button color="warning" onClick={() => history.push(`${url}/edit`)}>
                        EDIT
                      </Button>
                      <GeneratePDF body={body} head={columns} slip={slip} text="Payslip PDF" />
                    </div>
                  </div>
                </section>
              </div>
            )}
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default Payslip;
