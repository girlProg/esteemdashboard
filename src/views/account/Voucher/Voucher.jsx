import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Datatable from 'react-bs-datatable';
import { Button } from 'reactstrap';
import { css } from 'emotion';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { formatNumber } from '../../../components/account/Utils/format';
import GeneratePDF from '../../../components/account/Voucher/GeneratePDF';

export default function Voucher({ schoolBranch, month, year, allPayslips, varAdj, allVarAdjType }) {
  const history = useHistory();
  const { url } = useRouteMatch();

  const head = [
    { title: 'S/N', prop: 'sn', sortable: true, filterable: false },
    {
      title: 'View',
      prop: 'action',
      cell: row =>
        row.name !== 'TOTAL' && (
          <Button className="btn-sm" color="primary" onClick={() => handleViewPayslip(row)}>
            View Payslip
          </Button>
        )
    },
    {
      title: 'Edit',
      prop: 'edit',
      cell: row =>
        row.name !== 'TOTAL' && (
          <Button className="btn-sm" color="warning" onClick={() => handlePayslipEdit(row)}>
            EDIT
          </Button>
        )
    },
    { title: 'Name', prop: 'name', sortable: true, filterable: true },
    {
      title: 'Basic',
      prop: 'basic',
      sortable: true,
      filterable: false
    },
    {
      title: 'Housing',
      prop: 'housing',
      sortable: true,
      filterable: false
    },
    {
      title: 'Transport',
      prop: 'transport',
      sortable: true,
      filterable: false
    },
    {
      title: 'Meal',
      prop: 'meal',
      sortable: true,
      filterable: false
    },
    {
      title: 'Utility',
      prop: 'utility',
      sortable: true,
      filterable: false
    },
    {
      title: 'Education',
      prop: 'education',
      sortable: true,
      filterable: false
    },
    {
      title: 'Dressing',
      prop: 'dressing',
      sortable: true,
      filterable: false
    },
    {
      title: 'Medical',
      prop: 'medical',
      sortable: true,
      filterable: false
    },
    {
      title: 'Salary Amount',
      prop: 'salaryAmount',
      sortable: true,
      filterable: false
    },
    {
      title: 'Allowance for Heads',
      prop: 'allowanceforHeads',
      sortable: true,
      filterable: false
    },
    {
      title: 'Gross Income',
      prop: 'grossIncome',
      sortable: true,
      filterable: false
    },
    {
      title: 'Pension',
      prop: 'pension',
      sortable: true,
      filterable: false
    },
    { title: 'NHIS', prop: 'nhis', sortable: true, filterable: false },
    { title: 'Tax', prop: 'tax', sortable: true, filterable: false },
    {
      title: 'Credit to Bank',
      prop: 'credittobank',
      sortable: true,
      filterable: false
    }
  ];

  const varadjsformonth = new Set();

  const handleViewPayslip = row => {
    if (allPayslips.results) {
      allPayslips.results.forEach(slip => {
        if (slip.id === row.id) {
          history.push(`${url}/payslip/${row.id}`);
        }
      });
    }
  };

  const handlePayslipEdit = row => {
    allPayslips.results.forEach(slip => {
      if (slip.id === row.id) {
        history.push(`${url}/payslip/${row.id}/edit`, { from: history.location.pathname });
      }
    });
  };

  // add variable adjustments headers to voucher table
  useEffect(() => {
    if (allVarAdjType) {
      if (varadjsformonth !== undefined) {
        varadjsformonth.forEach(adj => {
          head.push({
            title: adj,
            prop: adj,
            sortable: true,
            filterable: false
          });
        });
      }
    }
    // setHeader(head);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [allVarAdjType]);

  function getTableBody() {
    const body = [];
    const sortedList = allPayslips?.results?.sort((a, b) => {
      if (a.staff.name < b.staff.name) {
        return -1;
      }
      return 1;
    });

    let sn = 0;

    function pushToBody(slip) {
      body.push({
        sn: (sn += 1),
        id: slip.id,
        name: slip.staff.name,
        basic: formatNumber(slip.basic[0].amount),
        housing: formatNumber(slip.housing[0] ? slip.housing[0].amount : ''),
        transport: formatNumber(slip.transport[0].amount),
        meal: formatNumber(slip.meal[0].amount),
        utility: formatNumber(slip.utility[0].amount),
        education: formatNumber(slip.education[0].amount),
        dressing: formatNumber(slip.dressing[0].amount),
        medical: formatNumber(slip.medical[0].amount),
        pension: formatNumber(slip.pension[0].amount),
        tax: formatNumber(slip.tax),
        credittobank: formatNumber(slip.credittobank),
        salaryAmount: formatNumber(slip.salaryAmount),
        grossIncome: formatNumber(slip.grossIncome),
        nhis: formatNumber(slip.nhis),
        allowanceforHeads: formatNumber(slip.allowanceforHeads),
        bankAccount: slip.staff.bankAccount[0] && slip.staff.bankAccount[0].number
      });
    }

    // filter by school branch
    sortedList &&
      sortedList.forEach(slip => {
        if (Number(slip.month) === month && Number(slip.year) === year) {
          if (schoolBranch.id === 'all') {
            pushToBody(slip);
          } else if (slip.staff.schoolBranch.id === Number(schoolBranch.id)) {
            pushToBody(slip);
          }
        }
      });

    // add variable adjustments to appropriate staff's payslip
    if (varAdj?.results) {
      body.forEach(slip => {
        varAdj.results.forEach(adj => {
          if (adj.payslip === slip.id) {
            const { name } = adj.name;
            slip[name] = formatNumber(adj.amount);
            varadjsformonth.add(adj.name.name);
          }
        });
      });
    }

    // calculate sum
    const varAdjArr = new Array(...varadjsformonth);
    const totalSum = {};

    // create an array for each keys in a payslip
    body.forEach(el => {
      // eslint-disable-next-line
      Object.entries(el).map(e => {
        if (e[0] !== 'id' && e[0] !== 'sn' && e[0] !== 'name') {
          totalSum[e[0]] = [];
        }
      });
    });

    // push values to their corresponding array
    body.forEach(el => {
      totalSum.basic.push(el.basic);
      totalSum.housing.push(el.housing);
      totalSum.transport.push(el.transport);
      totalSum.meal.push(el.meal);
      totalSum.utility.push(el.utility);
      totalSum.education.push(el.education);
      totalSum.dressing.push(el.dressing);
      totalSum.medical.push(el.medical);
      totalSum.pension.push(el.pension);
      totalSum.tax.push(el.tax);
      totalSum.credittobank.push(el.credittobank);
      totalSum.salaryAmount.push(el.salaryAmount);
      totalSum.grossIncome.push(el.grossIncome);
      totalSum.nhis.push(el.nhis);
      totalSum.allowanceforHeads.push(el.allowanceforHeads);
      varAdjArr.forEach(adj => {
        if (totalSum[adj]) {
          if (el[adj] !== undefined) {
            totalSum[adj].push(el[adj]);
          }
        }
      });
    });

    // sum function
    function sumColumn(array) {
      const cleanArr = array.filter(val => val !== undefined);
      const toInteger = cleanArr.map(val => Number(val.replace(',', '')));
      const total = toInteger.reduce((sum, num) => sum + num, 0).toFixed(2);
      return formatNumber(total);
    }

    if (body.length) {
      body.push({
        name: 'TOTAL',
        basic: sumColumn(totalSum.basic),
        housing: sumColumn(totalSum.housing),
        transport: sumColumn(totalSum.transport),
        meal: sumColumn(totalSum.meal),
        utility: sumColumn(totalSum.utility),
        education: sumColumn(totalSum.education),
        dressing: sumColumn(totalSum.dressing),
        medical: sumColumn(totalSum.medical),
        pension: sumColumn(totalSum.pension),
        tax: sumColumn(totalSum.tax),
        credittobank: sumColumn(totalSum.credittobank),
        salaryAmount: sumColumn(totalSum.salaryAmount),
        grossIncome: sumColumn(totalSum.grossIncome),
        nhis: sumColumn(totalSum.nhis),
        allowanceforHeads: sumColumn(totalSum.allowanceforHeads)
      });
      // eslint-disable-next-line
      body.filter(el => {
        if (el.name === 'TOTAL') {
          varAdjArr.forEach(adj => {
            if (totalSum[adj]) {
              el[adj] = sumColumn(totalSum[adj]);
            }
          });
        }
      });
    }

    return body;
  }

  const classes = {
    theadCol: css`
      .table-datatable__root & {
        font-weight: bold;
      }
    `,
    tbodyRow: css`
      width: auto;
      &:hover {
        background: #eaeaea;
      }
      &:last-child {
        font-weight: bold;
      }
    `,
    table: css`
      display: block;
      overflow-x: auto;
      white-space: nowrap;
      margin-top: 2em;
      font-weight: 400;
    `
  };

  return (
    <>
      {getTableBody().length > 0 && (
        <div className="d-flex justify-content-end" style={{ position: 'relative', top: -50 }}>
          <GeneratePDF
            body={getTableBody()}
            head={head}
            month={month}
            year={year}
            config="voucher"
            text="Voucher PDF"
            varAdjArr={[...varadjsformonth]}
          />
          <GeneratePDF body={getTableBody()} month={month} year={year} config="bank" text="Bank Report PDF" />
        </div>
      )}

      <Datatable tableHeaders={head} tableBody={getTableBody()} classes={classes} />
    </>
  );
}

Voucher.propTypes = {
  schoolBranch: PropTypes.object.isRequired,
  month: PropTypes.number.isRequired,
  year: PropTypes.number.isRequired
};
