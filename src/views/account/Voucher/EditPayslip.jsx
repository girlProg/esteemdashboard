import React, { useState, useEffect } from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col, Button } from 'reactstrap';
import classnames from 'classnames';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import Toast from '../../../components/account/Utils/Toast';
import Loading from '../../../components/account/Utils/Loading';
import AddVariable from '../../../components/account/Voucher/AddVariable';
import DeleteVariable from '../../../components/account/Voucher/DeleteVariable';
import EditVariable from '../../../components/account/Voucher/EditVariable';

export default function EditPayslip() {
  const { id } = useParams();
  const history = useHistory();
  const dispatch = useDispatch();

  const { loading, status, payslip, varAdj, allVarAdjType } = useSelector(state => state.staffReducer);
  const slipProfile = payslip?.results?.find(slip => slip.id === Number(id));
  const [showToast, setShowToast] = useState(false);
  const [toast, setToast] = useState({ type: '', message: '' });

  const [activeTab, setActiveTab] = useState('1');

  const monthYear = new Date(localStorage.getItem('monthYear'));
  const month = monthYear.getMonth() + 1;
  const year = monthYear.getFullYear();

  useEffect(() => {
    async function fetchData() {
      await actions.getVarAdj(dispatch, month, year);
      await actions.getPayslip(dispatch, month, year);
      actions.getVarAdjType(dispatch);
    }
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (status === 200) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Variable edited successfully.' });
      // history.go(0);
    }

    if (status === 201) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Variable added successfully.' });
      // history.go(0);
    }

    if (status === 204) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Variable deleted successfully.' });
      // history.go(0);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  const toggle = tab => {
    if (activeTab !== tab) {
      setActiveTab(tab);
    }
  };

  const prevLocation = history.location.state?.from;
  const backText = prevLocation?.endsWith('voucher') ? 'Voucher' : 'Payslip';

  return (
    <div>
      <div className="content mt-5">
        <Row>
          {loading ? (
            <Loading />
          ) : (
            <Col xs={12} md={9}>
              <div className="col-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title ml-0">
                      {`${monthYear.toLocaleString('default', {
                        month: 'long'
                      })}, ${monthYear.getFullYear()}
                      PAYSLIP EDIT`}
                    </h2>
                    <Button color="primary" className="float-right mt-3" onClick={() => history.goBack()}>
                      <i className="i-arrow-left-circle"></i> &nbsp; <span>Back to {backText}</span>
                    </Button>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      <div className="col-lg-12 mt-4">
                        {slipProfile ? (
                          <>
                            <h4>{slipProfile?.staff.name.toUpperCase()}</h4>
                            <div>
                              <Nav tabs>
                                {/* ADD VARIABLE TAB */}
                                <NavItem>
                                  <NavLink
                                    className={classnames({
                                      active: activeTab === '1'
                                    })}
                                    onClick={() => {
                                      toggle('1');
                                    }}
                                  >
                                    <i className="i-plus"></i> ADD VARIABLE
                                  </NavLink>
                                </NavItem>
                                {/* EDIT VARIABLE TAB */}
                                <NavItem>
                                  <NavLink
                                    className={classnames({
                                      active: activeTab === '2'
                                    })}
                                    onClick={() => {
                                      toggle('2');
                                    }}
                                  >
                                    <i className="i-pencil"></i> EDIT VARIABLE
                                  </NavLink>
                                </NavItem>
                                {/* DELETE VARIABLE TAB */}
                                <NavItem>
                                  <NavLink
                                    className={classnames({
                                      active: activeTab === '3'
                                    })}
                                    onClick={() => {
                                      toggle('3');
                                    }}
                                  >
                                    <i className="i-trash"></i> DELETE VARIABLE
                                  </NavLink>
                                </NavItem>
                              </Nav>
                              {/* CONTENT */}
                              <TabContent activeTab={activeTab}>
                                {/* ADD VARIABLE CONTENT */}
                                <TabPane tabId="1">
                                  <Row>
                                    <Col sm="12">
                                      <AddVariable slip={slipProfile} allVarAdjType={allVarAdjType} />
                                    </Col>
                                  </Row>
                                </TabPane>
                                {/* EDIT VARIABLE CONTENT */}
                                <TabPane tabId="2">
                                  <Row>
                                    <Col sm="12">
                                      <EditVariable slip={slipProfile} varAdj={varAdj} />
                                    </Col>
                                  </Row>
                                </TabPane>
                                {/* DELETE VARIABLE CONTENT */}
                                <TabPane tabId="3">
                                  <Row>
                                    <Col sm="12">
                                      <DeleteVariable slip={slipProfile} varAdj={varAdj} />
                                    </Col>
                                  </Row>
                                </TabPane>
                              </TabContent>
                            </div>
                          </>
                        ) : (
                          <h5 className="text-center">Payslip not available for this staff</h5>
                        )}
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </Col>
          )}
          {showToast && <Toast type={toast.type} message={toast.message} />}
        </Row>
      </div>
    </div>
  );
}
