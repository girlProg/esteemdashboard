/* eslint-disable no-nested-ternary */
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import { Row, Col, Dropdown, DropdownItem, DropdownMenu, DropdownToggle, Label } from 'reactstrap';
import { actions } from '../../../store/actions/staffActions';
import Voucher from './Voucher';
import Loading from '../../../components/account/Utils/Loading';

export default function VoucherHome() {
  const dispatch = useDispatch();
  const { allSchoolBranch, payslip, varAdj, allVarAdjType } = useSelector(state => state.staffReducer);

  const [monthYear, setMonthYear] = useState(new Date());
  localStorage.setItem('monthYear', monthYear);
  const [dropdownOpen, setDropdownOpen] = useState(false);
  const month = monthYear.getMonth() + 1;
  const year = monthYear.getFullYear();
  const [schoolBranch, setSchoolBranch] = useState({
    id: 'all',
    name: 'Show all'
  });

  useEffect(() => {
    if (!allSchoolBranch) actions.getAllSchoolBranch(dispatch);
    if (!allVarAdjType) actions.getVarAdjType(dispatch);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    actions.getVarAdj(dispatch, month, year);
    actions.getPayslip(dispatch, month, year);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [month, year]);

  const toggle = () => setDropdownOpen(prevState => !prevState);

  const dropDownSelect = e => {
    const { innerText, value } = e.target;
    setSchoolBranch({ id: value, name: innerText });
  };

  return (
    <div>
      <div className="content">
        <Row>
          <Col xs={12} md={12}>
            <div className="page-title">
              <div className="float-left">
                <h1 className="title">VOUCHER</h1>
                <div className="ml-3 d-flex">
                  <DatePicker
                    selected={monthYear}
                    onChange={date => {
                      setMonthYear(date);
                      localStorage.setItem('monthYear', date);
                    }}
                    dateFormat="MM/yyyy"
                    showMonthYearPicker
                    placeholderText="Click to select a month"
                  />
                  <div className="form-group col-md-6" style={{ marginTop: '-30px' }}>
                    <Label>Filter by school branch</Label>
                    <Dropdown className="d-block ml-0" isOpen={dropdownOpen} toggle={toggle}>
                      <DropdownToggle color={schoolBranch.name ? 'primary' : 'secondary'} caret>
                        {schoolBranch.name}
                      </DropdownToggle>
                      <DropdownMenu>
                        <DropdownItem value="all" onClick={dropDownSelect}>
                          Show All
                        </DropdownItem>
                        <DropdownItem divider></DropdownItem>
                        {allSchoolBranch?.results.map(branch => (
                          <div key={branch.id}>
                            <DropdownItem value={branch.id} onClick={dropDownSelect}>
                              {branch.name}
                            </DropdownItem>
                            <DropdownItem divider></DropdownItem>
                          </div>
                        ))}
                      </DropdownMenu>
                    </Dropdown>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-12">
              {payslip?.results?.length > 0 ? (
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title float-left">
                      {`VOUCHER FOR THE MONTH OF ${monthYear.toLocaleString('default', {
                        month: 'long'
                      })}, ${monthYear.getFullYear()}`}
                    </h2>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      <div className="col-lg-12 dt-disp">
                        <Voucher
                          allPayslips={payslip}
                          varAdj={varAdj}
                          allVarAdjType={allVarAdjType}
                          schoolBranch={schoolBranch}
                          month={month}
                          year={year}
                        />
                      </div>
                    </div>
                  </div>
                </section>
              ) : (
                <Loading />
              )}
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
}
