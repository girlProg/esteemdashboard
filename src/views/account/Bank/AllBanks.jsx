import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import Toast from '../../../components/account/Utils/Toast';
import BankList from '../../../components/account/Bank/BankList';

export default function AllBanks() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { url } = useRouteMatch();
  const { banks, status, loading } = useSelector(state => state.staffReducer);
  const [selectedBank, setSelectedBank] = useState(null);
  const [showToast, setShowToast] = useState(false);
  const [toast, setToast] = useState({ type: '', message: '' });

  useEffect(() => {
    async function fetchData() {
      await actions.getAllBanks(dispatch);
    }
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (status === 200) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Bank edited successfully.' });
    }

    if (status === 201) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Bank added successfully.' });
    }

    if (status === 204) {
      setShowToast(true);
      setToast({ type: 'success', message: 'Bank deleted successfully.' });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [status]);

  const handleEdit = e => {
    const name = e.target.parentElement.parentElement.firstChild.textContent;
    const select = banks?.results.find(el => el.name === name);
    history.push(`${url}/edit/${select.id}`);
  };

  const handleDelete = async () => {
    await actions.deleteBank(dispatch, selectedBank);
    history.go(0);
  };

  const getSelectedBank = e => {
    const name = e.target.parentElement.parentElement.firstChild.textContent;
    const bank = banks.results.find(el => el.name === name);
    setSelectedBank(bank);
  };

  return (
    <div className="content">
      <BankList
        loading={loading}
        banks={banks}
        getSelectedBank={getSelectedBank}
        handleEdit={handleEdit}
        handleDelete={handleDelete}
      />
      {showToast && <Toast type={toast.type} message={toast.message} />}
    </div>
  );
}
