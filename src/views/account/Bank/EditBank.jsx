import React, { useState, useEffect } from 'react';
import { Row, Col, Button } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import Loading from '../../../components/account/Utils/Loading';

function EditBank() {
  const { id } = useParams();
  const history = useHistory();
  const dispatch = useDispatch();
  const { banks, loading } = useSelector(state => state.staffReducer);
  const selectedBank = banks?.results.find(bank => bank.id === Number(id));
  const [state, setState] = useState({});

  useEffect(() => {
    async function fetchData() {
      await actions.getAllBanks(dispatch);
    }
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setState({ id: selectedBank?.id, name: selectedBank?.name });
  }, [selectedBank]);

  const handleChange = e => {
    const { name, value } = e.target;
    setState({ ...state, [name]: value });
  };

  const handleEditSave = async e => {
    e.preventDefault();
    await actions.editBank(dispatch, state);
    history.goBack();
  };

  return (
    <div>
      <div className="content">
        <Row>
          {loading ? (
            <Loading />
          ) : (
            <Col xs={12} md={12}>
              <div className="page-title">
                <div className="float-left">
                  <h1 className="title">Edit Bank</h1>
                </div>
              </div>

              <div className="col-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title float-left">Bank</h2>
                    <Button color="primary" className="float-right mt-3" onClick={() => history.goBack()} size="sm">
                      <i className="i-arrow-left-circle"></i> &nbsp; <span>Back to list</span>
                    </Button>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
                        <form onSubmit={handleEditSave}>
                          <div className="form-row">
                            <div className="form-group col-md-12">
                              <label htmlFor="bank_name">Name</label>
                              <input
                                type="text"
                                required
                                className="form-control"
                                id="bank_name"
                                name="name"
                                value={state.name}
                                onChange={handleChange}
                              />
                            </div>
                          </div>
                          <button type="submit" className="btn btn-primary">
                            UPDATE
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </Col>
          )}
        </Row>
      </div>
    </div>
  );
}

export default EditBank;
