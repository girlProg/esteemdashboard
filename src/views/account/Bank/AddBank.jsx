import React, { useState } from 'react';
import { Row, Col } from 'reactstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { actions } from '../../../store/actions/staffActions';
import Loading from '../../../components/account/Utils/Loading';

function AddBank() {
  const dispatch = useDispatch();
  const history = useHistory();
  const { loading } = useSelector(state => state.staffReducer);

  const [state, setState] = useState({
    // eslint-disable-next-line prettier/prettier
    name: ''
  });

  const handleChange = e => {
    const { name, value } = e.target;
    setState({ [name]: value });
  };

  const handleSubmit = async e => {
    e.preventDefault();
    await actions.addBank(dispatch, state);
    history.push('bank');
  };

  return (
    <div>
      <div className="content">
        <Row>
          {loading ? (
            <Loading />
          ) : (
            <Col xs={12} md={12}>
              <div className="page-title">
                <div className="float-left">
                  <h1 className="title">Add Bank</h1>
                </div>
              </div>

              <div className="col-12">
                <section className="box ">
                  <header className="panel_header">
                    <h2 className="title float-left">Bank</h2>
                  </header>
                  <div className="content-body">
                    <div className="row">
                      <div className="col-12 col-sm-12 col-md-10 col-lg-10 col-xl-8">
                        <form onSubmit={handleSubmit}>
                          <div className="form-row">
                            <div className="form-group col-md-12">
                              <label htmlFor="bank_name">Name</label>
                              <input
                                type="text"
                                required
                                className="form-control"
                                id="bank_name"
                                name="name"
                                value={state.name}
                                onChange={handleChange}
                              />
                            </div>
                          </div>
                          <button type="submit" className="btn btn-primary">
                            SAVE
                          </button>
                        </form>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </Col>
          )}
        </Row>
      </div>
    </div>
  );
}

export default AddBank;
