const BaseURL = 'https://secret-hollows-02098.herokuapp.com/';
// const BaseURL = 'http://127.0.0.1:8004/';
const token = localStorage.getItem('token');

// const headers =   {
//   Accept: 'application/json',
// 'Content-Type': 'application/json',
//   Authorization: `Token ${token}`
// }

const headers = token
  ? {
      Accept: 'application/json',
      'Content-Type': 'application/json',
      Authorization: `Token ${token}`
    }
  : {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    };

const postReq = async (url, method, body) => {
  body = JSON.stringify(body);
  const rawResponse = await fetch(BaseURL + url, {
    method,
    headers,
    body
  });
  // const content = await rawResponse.json();
  return { rawResponse };
};

const getReq = async url => {
  const rawResponse = await fetch(BaseURL + url, { headers });
  const content = await rawResponse.json();
  return { rawResponse, content };
};

export const endpoints = {
  login: async (username, password) => {
    const response = await postReq('auth/token/login/', 'POST', {
      username,
      password
    });
    return response;
  },

  signup: async (username, password) => {
    const response = await postReq('auth/users/', 'POST', {
      username,
      password
    });
    return response;
  },

  getAllStaff: async () => {
    const response = await getReq(`api/staff/?limit=200`);
    return response;
  },

  editStaff: async staffObj => {
    const response = await postReq(`api/staff/${staffObj.id}/`, 'PUT', staffObj);
    return response;
  },

  deleteStaff: async staffObj => {
    const response = await postReq(`api/staff/${staffObj.id}/`, 'DELETE', staffObj);
    return response;
  },

  addStaff: async staffObj => {
    const response = await postReq(`api/staff/`, 'POST', staffObj);
    return response;
  },

  getAllPFAs: async () => {
    const response = await getReq(`api/pfas/`);
    return response;
  },

  addPfa: async pfaObj => {
    const response = await postReq(`api/pfas/`, 'POST', pfaObj);
    return response;
  },

  editPfa: async pfaObj => {
    const response = await postReq(`api/pfas/${pfaObj.id}/`, 'PUT', pfaObj);
    return response;
  },

  deletePfa: async pfaObj => {
    const response = await postReq(`api/pfas/${pfaObj.id}/`, 'DELETE', pfaObj);
    return response;
  },

  getAllBanks: async () => {
    const response = await getReq(`api/bank/`);
    return response;
  },

  addBank: async bankObj => {
    const response = await postReq(`api/bank/`, 'POST', bankObj);
    return response;
  },

  editBank: async bankObj => {
    const response = await postReq(`api/bank/${bankObj.id}/`, 'PUT', bankObj);
    return response;
  },

  deleteBank: async bankObj => {
    const response = await postReq(`api/bank/${bankObj.id}/`, 'DELETE', bankObj);
    return response;
  },

  addBankAccount: async bankAccountObj => {
    const response = await postReq(`api/bankaccount/`, 'POST', bankAccountObj);
    return response;
  },

  getAllPayslip: async () => {
    const response = await getReq(`api/payslip/`);
    return response;
  },

  getPayslip: async (month, year) => {
    const response = await getReq(`api/payslip?limit=200&month=${month}&year=${year}`);
    return response;
  },

  getAllSchoolBranch: async () => {
    const response = await getReq(`api/schoolbranch/`);
    return response;
  },

  addSchoolBranch: async schoolObj => {
    const response = await postReq(`api/schoolbranch/`, 'POST', schoolObj);
    return response;
  },

  editSchoolBranch: async schoolObj => {
    const response = await postReq(`api/schoolbranch/${schoolObj.id}/`, 'PUT', schoolObj);
    return response;
  },

  deleteSchoolBranch: async schoolObj => {
    const response = await postReq(`api/schoolbranch/${schoolObj.id}/`, 'DELETE', schoolObj);
    return response;
  },

  getVarAdj: async (month, year) => {
    const response = await getReq(`api/varadj?payslip__month=${month}&payslip__year=${year}`);
    return response;
  },

  addVarAdj: async varObj => {
    const response = await postReq(`api/varadj/`, 'POST', varObj);
    return response;
  },

  editVarAdj: async varObj => {
    const response = await postReq(`api/varadj/${varObj.id}/`, 'PUT', varObj);
    return response;
  },

  deleteVarAdj: async varObj => {
    const response = await postReq(`api/varadj/${varObj.id}/`, 'DELETE', varObj);
    return response;
  },

  getVarAdjType: async () => {
    const response = await getReq(`api/varadjtype/`);
    return response;
  },

  addVarAdjType: async varObj => {
    const response = await postReq(`api/varadjtype/`, 'POST', varObj);
    return response;
  },

  editVarAdjType: async varObj => {
    const response = await postReq(`api/varadjtype/${varObj.id}/`, 'PUT', varObj);
    return response;
  },

  deleteVarAdjType: async varObj => {
    const response = await postReq(`api/varadjtype/${varObj.id}/`, 'DELETE', varObj);
    return response;
  }
};
